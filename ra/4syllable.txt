What you don't know
Just don't, let's say,
This once: Don't think.
A step to climb.
I'm Sarah, and ...
Hello, Sarah.
Her mom and dad
But they were hurt."
To those you harmed?
Admit to gains
Against the muse.
Street hawkers call.
I love waking
On a hot road
Of word from word
Or time from thrust
From where you are
Spoke to your voice
But never seen.
Come across you
Decades later
And find again
To calm me when
Your photo pressed
I guess I missed.
The back, your name�
If more was meant,
It never came.
There were others
And yet, somehow
A rush of breath
It was no dream.
Like yesterday
Since long ago
In the shallows
To strain to leap
Nevertheless
Into the cold
Water and tie
Ankle in case
Interior
Winking whitely
Of an oyster
Still stalks the eaves
Tried, that white men
The river banks
Its banks, quiver's
Noiseless, easy
Wet ripe apple's
Leather. To aim
Is a shaft tipped
To hope to take
What's struck in hand,
Down damage by,
Turned under wings
Wind tests tremor
This grammar's time
To aim to pierce
Flesh. Hope's arrow's
Feather's fletching
The young husband
Because no, they've
Talk finally.
Dies unburied
During rainstorms.
Or donated
A knife is honed
And when it rusts,
And Mi Ditmar
These beasts and birds;
These tall grasses
And yet, how full
The universe�
The very things
Therefore, he sang
[The life I live]
The life I live,
The one I hoped
They coincide.
Sometimes, briefly,
From Abilene
To be exact),
On being not
A steamroller
Smack-dab over
A fat black snake.
Up surged a cheer
Whisker twitches
The fat black snake
As the asphalt
To know was love.
But what moved me
Flooded Meadow
All the way through.
Landscape of Night
Spreads itself thin
Fills up low shoes
And microbes gorge
Hard to praise. Love
See for ourselves
Him with a horde
Though he'd looked hard,
Though, to him. Dread
Revealed like words
All drinks are free!�
The tree releafed!
Slow rain of flesh.
Strong like earthworms
Milder poisons.
That must be fed,
Such brains belong
Who mow their lawns,
Wear shoes that hurt,
Who stay up late,
Speed on highways,
When you folded
First east, then west,
And made for me
Drenched in the light.
That had nested
Pangs of dryness,
Of the canyon
A canyon wren
And a coyote
As we descend
The eastern wall
Ponderosa
Of a merlin
In the meadow
Weary fears, the
Through vines, so plush
Their seeds and flesh
All bite-size blush
Drawing Her Bath
Can't you hear that?
What's beautiful?
Which you still wear.
To the evening.
Backyards Brooklyn
My happy sense
A road in rain?
Toward the pines.
Of something small,
Or my rifle,
Blood in a thing�
The morning dew�
To each child's wrist.
Where have you been
Would work for me
To me, either
The paperboy
My children's heads
For years. I've sunk
Of some poor goose�
Blood. I believed
Twitch. A body's
Crossed wires. The lost
Nerves have never
We saw the brief
And tried to sip
A single one
From the magic
Mute button pressed
And snap the air.
Dangerous butch
Uncamouflaged?
Or live or die,
To receipts which
A compassion
His weakest lamb.
Many thousands
A simple scene:
And Uncle Vance
That as a kid
And sang so you
He buried Blue
Who never drew,
I crossed the Cross.
But he can't see
But soon's not here.
We once came from.
If we are one
The stars below
Are weeping snow
Mirror Dweller
We do not ask
And falls on me.
Is not shelter.
Against laughter,
Keeps the house cool.
But they watch us.
Don't press too far.
It's pure release.
Choose this embrace,
Coal by the bag,
Cinders and slag,
As snow and watch
Stygian, ashed�
Reburnishing
To invoke the
Dismantle us.
May, permission
The cottonwoods,
Charge and a share
With what you've done,
All We like Sheep
That my peach turned
Reserved for those
In mourning for
Acts of insight
They inhale clouds
She is clothed in
Muster green, leaf
Dust, ashes, build
Not walk on air
They howl at it.
We howl at it,
I was a wolf.
The drums don't stop,
40 years ago, 40 years . . .
Push on, push on.
Into the sweet
Fleshed, black and gray
Around the tough
His knife to make
From the oyster�s
Gland no bigger
Flesh. Both hosts eased
So that one pearl
By what you think
Is pain. Nothing
Production: you
Four-chambered heart,
Close? Imagine
The thing we want
And ride on top.
Spraying thistles.
Apart from us,
Pain's fountaining,
Is streaked by real
Like denizens
Caterpillars
No No, no Yes,
Moved bed and tears�
Devastations
Devastations�
Smooshed to humus�
Devastations�
Devastations�
Was thus fulfilled.
Nor your choice of
Paint and lacquer
For the launch of
Your ambition
Nor the dancing
Will ever be
Restored to you
By the people
In the topmost
It may yet from
More than she thought.
Make estranging
Where the report
And she tended
Cloth of enough
The lettering
But not stating
Her hands in dirt
Feeling for roots
They no longer
Tending, even
Hung thick falling.
Punctured/shingling
SECOND GARDEN
He cannot look
These are funny
SEVENTH GARDEN
Beneath the ice
These things contained,
One thing to blame
But is still there,
To be strengthened.
Consolations�
The throat's barked lock.
And tea-trees, leaned
The kind brewing
Change, and the hair
Many forms of
Doubleness serve
Lulling us to
The essential
Focal baffling
Inherent in
Experience
Keep happening.
Without a stir.
In the darkness
Up from the beach
The sky draws close,
Your tongue asleep
Blond ponytail,
To the black ground.
Among the blooms.
Blood when it spills
Come on gurneys
The Performance
All the best truths
By years of light
Breakups the break
In a green tree
And leisurely
Three bright green birds
Splashed with their lime.
They bob and sway
These do not care,
I learn to swim.
A flyover
Timpani is
Is happening
The small coiled teeth
Seen correctly
Out of blue sky
Then uncount them
Light and darkness
Before it's time
It isn't smooth.
This shard along.
But to a first
Or your nostrils
Shadow of wings
What's between us
Is made of clay,
Becomes the task.
With powdered gold.
Sometimes the joins
The Stag at Eve
Of idea?
And runic stones,
In art. Coleridge
Is "Kubla Khan."
Kubla was king
A palace called
Both at the same
And anyway,
Book of English
Verse, used to be
Mine, and now it's
With the sun's glare
When all love wants�
A few stray notes,
And stand in line.
Droning. Hello,
Too uppity,
I watch the sound
You want. I'll wait.
Just as the word
Is mostly end,
We've reached the path
Where brittle leaves
Crack underfoot,
That we can break
So many times
Leaves, to pluck what
Unpleasant sting
Of being healed,
The after-war
Deep in the once
How many wars
Winter's the thing.
To sleep at last
In the distance.
Winter's the thing
"Come out and play,"
The song's refrain
My heart, your eyes
Sunshine on oil,
Scatter-brained word.
Shattered-pane world?
With night, starbit,
And both of them
Facing off. En-
Igma tipped to
She floats on swirled
Each other. Stars
Don't look away
She against his
February, 1865�
Ash in the rain
They're rehearsing
Among insects.
In the sunlight.
Chill April.
And slips further,
Into my eye
There is my friend
She would know that
I know what's next�
Monk has to stay
And breathe again.
Trees. A quickness,
Wordless at first,
Spring the winter-
Lumened limit
Something human
Would not be reached
One-two, one-two
What you don't know
Just don't, let's say,
This once: Don't think.
A step to climb.
I'm Sarah, and ...
Hello, Sarah.
Her mom and dad
But they were hurt."
To those you harmed?
Admit to gains
Against the muse.
Street hawkers call.
I love waking
On a hot road
Of word from word
Or time from thrust
From where you are
Spoke to your voice
But never seen.
Come across you
Decades later
And find again
To calm me when
Your photo pressed
I guess I missed.
The back, your name�
If more was meant,
It never came.
There were others
And yet, somehow
A rush of breath
It was no dream.
Like yesterday
Since long ago
In the shallows
To strain to leap
Nevertheless
Into the cold
Water and tie
Ankle in case
Interior
Winking whitely
Of an oyster
Still stalks the eaves
Tried, that white men
The river banks
Its banks, quiver's
Noiseless, easy
Wet ripe apple's
Leather. To aim
Is a shaft tipped
To hope to take
What's struck in hand,
Down damage by,
Turned under wings
Wind tests tremor
This grammar's time
To aim to pierce
Flesh. Hope's arrow's
Feather's fletching
The young husband
Because no, they've
Talk finally.
Dies unburied
During rainstorms.
Or donated
A knife is honed
And when it rusts,
And Mi Ditmar
These beasts and birds;
These tall grasses
And yet, how full
The universe�
The very things
Therefore, he sang
[The life I live]
The life I live,
The one I hoped
They coincide.
Sometimes, briefly,
From Abilene
To be exact),
On being not
A steamroller
Smack-dab over
A fat black snake.
Up surged a cheer
Whisker twitches
The fat black snake
As the asphalt
To know was love.
But what moved me
Flooded Meadow
All the way through.
Landscape of Night
Spreads itself thin
Fills up low shoes
And microbes gorge
Hard to praise. Love
See for ourselves
Him with a horde
Though he'd looked hard,
Though, to him. Dread
Revealed like words
All drinks are free!�
The tree releafed!
Slow rain of flesh.
Strong like earthworms
Milder poisons.
That must be fed,
Such brains belong
Who mow their lawns,
Wear shoes that hurt,
Who stay up late,
Speed on highways,
When you folded
First east, then west,
And made for me
Drenched in the light.
That had nested
Pangs of dryness,
Of the canyon
A canyon wren
And a coyote
As we descend
The eastern wall
Ponderosa
Of a merlin
In the meadow
Weary fears, the
Through vines, so plush
Their seeds and flesh
All bite-size blush
Drawing Her Bath
Can't you hear that?
What's beautiful?
Which you still wear.
To the evening.
Backyards Brooklyn
My happy sense
A road in rain?
Toward the pines.
Of something small,
Or my rifle,
Blood in a thing�
The morning dew�
To each child's wrist.
Where have you been
Would work for me
To me, either
The paperboy
My children's heads
For years. I've sunk
Of some poor goose�
Blood. I believed
Twitch. A body's
Crossed wires. The lost
Nerves have never
We saw the brief
And tried to sip
A single one
From the magic
Mute button pressed
And snap the air.
Dangerous butch
Uncamouflaged?
Or live or die,
To receipts which
A compassion
His weakest lamb.
Many thousands
A simple scene:
And Uncle Vance
That as a kid
And sang so you
He buried Blue
Who never drew,
I crossed the Cross.
But he can't see
But soon's not here.
We once came from.
If we are one
The stars below
Are weeping snow
Mirror Dweller
We do not ask
And falls on me.
Is not shelter.
Against laughter,
Keeps the house cool.
But they watch us.
Don't press too far.
It's pure release.
Choose this embrace,
Coal by the bag,
Cinders and slag,
As snow and watch
Stygian, ashed�
Reburnishing
To invoke the
Dismantle us.
May, permission
The cottonwoods,
Charge and a share
With what you've done,
All We like Sheep
That my peach turned
Reserved for those
In mourning for
Acts of insight
They inhale clouds
She is clothed in
Muster green, leaf
Dust, ashes, build
Not walk on air
They howl at it.
We howl at it,
I was a wolf.
The drums don't stop,
40 years ago, 40 years . . .
Push on, push on.
Into the sweet
Fleshed, black and gray
Around the tough
His knife to make
From the oyster�s
Gland no bigger
Flesh. Both hosts eased
So that one pearl
By what you think
Is pain. Nothing
Production: you
Four-chambered heart,
Close? Imagine
The thing we want
And ride on top.
Spraying thistles.
Apart from us,
Pain's fountaining,
Is streaked by real
Like denizens
Caterpillars
No No, no Yes,
Moved bed and tears�
Devastations
Devastations�
Smooshed to humus�
Devastations�
Devastations�
Was thus fulfilled.
Nor your choice of
Paint and lacquer
For the launch of
Your ambition
Nor the dancing
Will ever be
Restored to you
By the people
In the topmost
It may yet from
More than she thought.
Make estranging
Where the report
And she tended
Cloth of enough
The lettering
But not stating
Her hands in dirt
Feeling for roots
They no longer
Tending, even
Hung thick falling.
Punctured/shingling
SECOND GARDEN
He cannot look
These are funny
SEVENTH GARDEN
Beneath the ice
These things contained,
One thing to blame
But is still there,
To be strengthened.
Consolations�
The throat's barked lock.
And tea-trees, leaned
The kind brewing
Change, and the hair
Many forms of
Doubleness serve
Lulling us to
The essential
Focal baffling
Inherent in
Experience
Keep happening.
Without a stir.
In the darkness
Up from the beach
The sky draws close,
Your tongue asleep
Blond ponytail,
To the black ground.
Among the blooms.
Blood when it spills
Come on gurneys
The Performance
All the best truths
By years of light
Breakups the break
In a green tree
And leisurely
Three bright green birds
Splashed with their lime.
They bob and sway
These do not care,
I learn to swim.
A flyover
Timpani is
Is happening
The small coiled teeth
Seen correctly
Out of blue sky
Then uncount them
Light and darkness
Before it's time
It isn't smooth.
This shard along.
But to a first
Or your nostrils
Shadow of wings
What's between us
Is made of clay,
Becomes the task.
With powdered gold.
Sometimes the joins
The Stag at Eve
Of idea?
And runic stones,
In art. Coleridge
Is "Kubla Khan."
Kubla was king
A palace called
Both at the same
And anyway,
Book of English
Verse, used to be
Mine, and now it's
With the sun's glare
When all love wants�
A few stray notes,
And stand in line.
Droning. Hello,
Too uppity,
I watch the sound
You want. I'll wait.
Just as the word
Is mostly end,
We've reached the path
Where brittle leaves
Crack underfoot,
That we can break
So many times
Leaves, to pluck what
Unpleasant sting
Of being healed,
The after-war
Deep in the once
How many wars
Winter's the thing.
To sleep at last
In the distance.
Winter's the thing
"Come out and play,"
The song's refrain
My heart, your eyes
Sunshine on oil,
Scatter-brained word.
Shattered-pane world?
With night, starbit,
And both of them
Facing off. En-
Igma tipped to
She floats on swirled
Each other. Stars
Don't look away
She against his
February, 1865�
Ash in the rain
They're rehearsing
Among insects.
In the sunlight.
Chill April.
And slips further,
Into my eye
There is my friend
She would know that
I know what's next�
Monk has to stay
And breathe again.
Trees. A quickness,
Wordless at first,
Spring the winter-
Lumened limit
Something human
Would not be reached
One-two, one-two
And Mi Ditmar
Spring the winter-
Lumened limit
Something human
Would not be reached
One-two, one-two
Spring the winter-
Lumened limit
Something human
Would not be reached
One-two, one-two
What you don't know
Just don't, let's say,
This once: Don't think.
A step to climb.
I'm Sarah, and ...
Hello, Sarah.
Her mom and dad
But they were hurt."
To those you harmed?
Admit to gains
Against the muse.
Street hawkers call.
I love waking
On a hot road
Of word from word
Or time from thrust
From where you are
Spoke to your voice
But never seen.
Come across you
Decades later
And find again
To calm me when
Your photo pressed
I guess I missed.
The back, your name�
If more was meant,
It never came.
There were others
And yet, somehow
A rush of breath
It was no dream.
Like yesterday
Since long ago
In the shallows
To strain to leap
Nevertheless
Into the cold
Water and tie
Ankle in case
Interior
Winking whitely
Of an oyster
Still stalks the eaves
Tried, that white men
The river banks
Its banks, quiver's
Noiseless, easy
Wet ripe apple's
Leather. To aim
Is a shaft tipped
To hope to take
What's struck in hand,
Down damage by,
Turned under wings
Wind tests tremor
This grammar's time
To aim to pierce
Flesh. Hope's arrow's
Feather's fletching
The young husband
Because no, they've
Talk finally.
Dies unburied
During rainstorms.
Or donated
A knife is honed
And when it rusts,
And Mi Ditmar
These beasts and birds;
These tall grasses
And yet, how full
The universe�
The very things
Therefore, he sang
[The life I live]
The life I live,
The one I hoped
They coincide.
Sometimes, briefly,
From Abilene
To be exact),
On being not
A steamroller
Smack-dab over
A fat black snake.
Up surged a cheer
Whisker twitches
The fat black snake
As the asphalt
To know was love.
But what moved me
Flooded Meadow
All the way through.
Landscape of Night
Spreads itself thin
Fills up low shoes
And microbes gorge
Hard to praise. Love
See for ourselves
Him with a horde
Though he'd looked hard,
Though, to him. Dread
Revealed like words
All drinks are free!�
The tree releafed!
Slow rain of flesh.
Strong like earthworms
Milder poisons.
That must be fed,
Such brains belong
Who mow their lawns,
Wear shoes that hurt,
Who stay up late,
Speed on highways,
When you folded
First east, then west,
And made for me
Drenched in the light.
That had nested
Pangs of dryness,
Of the canyon
A canyon wren
And a coyote
As we descend
The eastern wall
Ponderosa
Of a merlin
In the meadow
Weary fears, the
Through vines, so plush
Their seeds and flesh
All bite-size blush
Drawing Her Bath
Can't you hear that?
What's beautiful?
Which you still wear.
To the evening.
Backyards Brooklyn
My happy sense
A road in rain?
Toward the pines.
Of something small,
Or my rifle,
Blood in a thing�
The morning dew�
To each child's wrist.
Where have you been
Would work for me
To me, either
The paperboy
My children's heads
For years. I've sunk
Of some poor goose�
Blood. I believed
Twitch. A body's
Crossed wires. The lost
Nerves have never
We saw the brief
And tried to sip
A single one
From the magic
Mute button pressed
And snap the air.
Dangerous butch
Uncamouflaged?
Or live or die,
To receipts which
A compassion
His weakest lamb.
Many thousands
A simple scene:
And Uncle Vance
That as a kid
And sang so you
He buried Blue
Who never drew,
I crossed the Cross.
But he can't see
But soon's not here.
We once came from.
If we are one
The stars below
Are weeping snow
Mirror Dweller
We do not ask
And falls on me.
Is not shelter.
Against laughter,
Keeps the house cool.
But they watch us.
Don't press too far.
It's pure release.
Choose this embrace,
Coal by the bag,
Cinders and slag,
As snow and watch
Stygian, ashed�
Reburnishing
To invoke the
Dismantle us.
May, permission
The cottonwoods,
Charge and a share
With what you've done,
All We like Sheep
That my peach turned
Reserved for those
In mourning for
Acts of insight
They inhale clouds
She is clothed in
Muster green, leaf
Dust, ashes, build
Not walk on air
They howl at it.
We howl at it,
I was a wolf.
The drums don't stop,
40 years ago, 40 years . . .
Push on, push on.
Into the sweet
Fleshed, black and gray
Around the tough
His knife to make
From the oyster�s
Gland no bigger
Flesh. Both hosts eased
So that one pearl
By what you think
Is pain. Nothing
Production: you
Four-chambered heart,
Close? Imagine
The thing we want
And ride on top.
Spraying thistles.
Apart from us,
Pain's fountaining,
Is streaked by real
Like denizens
Caterpillars
No No, no Yes,
Moved bed and tears�
Devastations
Devastations�
Smooshed to humus�
Devastations�
Devastations�
Was thus fulfilled.
Nor your choice of
Paint and lacquer
For the launch of
Your ambition
Nor the dancing
Will ever be
Restored to you
By the people
In the topmost
It may yet from
More than she thought.
Make estranging
Where the report
And she tended
Cloth of enough
The lettering
But not stating
Her hands in dirt
Feeling for roots
They no longer
Tending, even
Hung thick falling.
Punctured/shingling
SECOND GARDEN
He cannot look
These are funny
SEVENTH GARDEN
Beneath the ice
These things contained,
One thing to blame
But is still there,
To be strengthened.
Consolations�
The throat's barked lock.
And tea-trees, leaned
Change, and the hair
Many forms of
Doubleness serve
Lulling us to
The essential
Focal baffling
Inherent in
Experience
Keep happening.
Without a stir.
In the darkness
Up from the beach
The sky draws close,
Your tongue asleep
Blond ponytail,
To the black ground.
Among the blooms.
Blood when it spills
Come on gurneys
The Performance
All the best truths
By years of light
Breakups the break
In a green tree
And leisurely
Three bright green birds
Splashed with their lime.
They bob and sway
These do not care,
I learn to swim.
A flyover
Timpani is
Is happening
The small coiled teeth
Seen correctly
Out of blue sky
Then uncount them
Light and darkness
Before it's time
It isn't smooth.
This shard along.
But to a first
Or your nostrils
Shadow of wings
What's between us
Is made of clay,
Becomes the task.
With powdered gold.
Sometimes the joins
The Stag at Eve
Of idea?
And runic stones,
In art. Coleridge
Is "Kubla Khan."
Kubla was king
A palace called
Both at the same
And anyway,
Book of English
Verse, used to be
Mine, and now it's
With the sun's glare
When all love wants�
A few stray notes,
And stand in line.
Droning. Hello,
Too uppity,
I watch the sound
You want. I'll wait.
Just as the word
Is mostly end,
We've reached the path
Where brittle leaves
Crack underfoot,
That we can break
So many times
Leaves, to pluck what
Unpleasant sting
Of being healed,
The after-war
Deep in the once
How many wars
Winter's the thing.
To sleep at last
In the distance.
Winter's the thing
"Come out and play,"
The song's refrain
My heart, your eyes
Sunshine on oil,
Scatter-brained word.
Shattered-pane world?
With night, starbit,
And both of them
Facing off. En-
Igma tipped to
She floats on swirled
Each other. Stars
Don't look away
She against his
February, 1865�
Ash in the rain
They're rehearsing
Among insects.
In the sunlight.
Chill April.
And slips further,
Into my eye
There is my friend
She would know that
I know what's next�
Monk has to stay
And breathe again.
Trees. A quickness,
Wordless at first,
Spring the winter-
Lumened limit
Something human
Would not be reached
One-two, one-two
Spring the winter-
Lumened limit
Something human
Would not be reached
One-two, one-two
