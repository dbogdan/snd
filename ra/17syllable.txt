Shahid: beloved in Persian, witness in Arabic, you drove back
No, not since yesterday    she taught me to be visible    then follow
Today I name the lasting roads:    artery    toll road   road of disguise
I took to the heat    like a dog to an electric fence    don't go past
And appreciated anew Italians' conceptions of love
The ice- and milt-stippled current; a sigh; the intervention of drift.
But moving forward as walking across it to get to the window
And wondering how she got out of the place she's usually in,
Calling up what never was in the tonal variations of gray
With a giant ribbon in my hair, the sorrow of living in time
Of fidelity and proximity, the latter is a watchword.
And flipping it forward and back in order to prove one theory
Deep in concentration, as I get closer I see she's just painting.
To be the type of person who goes to such places alone. It's her
Capitalizing proper nouns, stopping only to hurry forward,
I accept that her script is cuneiform and that a grave puzzle
Tall and undeniable, her back to us in the withering light.
From below an ear to the corner of a mouth, a smiley it's called,
Raised keloid tissue pale on darker skin. I remember as a kid
But it soon becomes apparent his wife and my husband are stewing,
But in the middle of my oysters I think of my great-grandfather,
To threaten that all my reading would destroy me, too, and I pictured
Bathsheba Everdene, was so rich and beautiful and stupid
Make an imperfect pomegranate-delivery system. It's tempting
To lie to the young. But you told us rightly about the beautiful
At last the animals starved by drought will eat the cactus, spines and all.
On an ice-cold morgue slab, smilingly shaking loose their beautiful hair.
It's hot hot in parts of Texas, but rain drowns Tennessee, people die.
You'd run into a hillbilly backwoods crazy, itching to kill you.
Bliss as sexual, and even after the curator mentioned sex
With all those lost spaces we try to recapture in our innocence
Then it goes away into the basement of old records that can't be
So much undone, or never done, or holding for a moment only,
So she'd be fresh she put on a little lipstick drawing on her smile
Apologize I let my sister succor those in need and suffer
The same jokes about eternity. New people join every day, of
Up ahead the kindly and the brutal alike keep disappearing.
And when she said she'd read a book about spontaneous combustion
A crime scene, reported by the driver on arrival in Phoenix
In the food co-op sighing as he shelves bags of Garden of Eatin'
We'd like to go on like this, Stan, but posthumously living uphill
From one life, and nearly reborn into the next, is taking its toll.
She had cut her toes on some glass in the high grass along the beach, too.
Rilke and George in hospital beds presumably, in Switzerland,
On the preexisting psychology). Exchange of equivalent gifts
Asserts a "rightness" in the universe�their word is an ascending
Invented especially for scenting land. Indeed, in one more day
With music and erotically bobbing hips. Is this the story
Of a soft-serve ice cream, and the family of seven laboring
And I put my arms around my son and told him, you have a future
I learned to see the world through the eye of a needle, to shape sticks,
To sing mourning songs. I thought myself the envy of the great teacher.
Then I believed for some years that humility was the great teacher.
One cannot know incompletion until completed. Newly in love,
We have plans for that house, for Nathan and for Cooper: today one draws,
The dancer-conqueror, and downs a slice of watermelon, red part
And the poison stars, exploding in their traces, cluster in the west.
And yet, Dear Ted, forgive me, but I need the boat of a bigger name:
Pinecones at the Villa Borghese: Fibonacci increments,
The Douglas-Fir can eschew standing in a fire which burns but does
Scrape the storm of its howl. Cellar as if to swarm, night as if to rot.
So archetypal in its split-level, clapboard ordinariness,
The sunlight gives the brick walls a ruddy, Hopper-esque gravitas?�
Self-abnegation of walking the streets of Natick, Massachusetts,
And there's a music of world-weary, self-extinguishing tragedy
Into a stiff-crested hoopoe with a pronged beak to replace his sword.
As we bear him, like an offering, towards the place I still call home,
One of them, as she loses her grip on the keel of that Cretan ship
Alice: the provenance, the gatekeeper. So it wasn't all hooey.
His second exoneural projecto-self in a pocket of subspace,
Photograph of two soldiers in camouflage battledress their slapdash
Of sun and sand and the faded brick pastels of walls roof-slates gables
Land of combat and they wary as cats ready for fight or flight and
Help the other back to base safe for one more day in which the basics
The moon snags the train of its wedding dress in the blackberry brambles.
The moon inhales the sluggish cloy of opium, exhales gypsum dust.
Water is only smooth like this on evenings of exceptional calm
It's a cup of no value, bought at a dry-goods-and-grocery shop
Honors the Holy Name. If you were exiled tomorrow, you would not
From gasoline comes internal combustion, comes a world of
Comes the pace of the century and its mode of transport and its
And afterwards he celebrates with a complimentary muskrat
And for this moment he is not worried about magneto coils or
It's picked up, throwing locutions like truly diabolical food
"You'll be a famous psychiatrist," I said to the academic.
Better to be the last chronicler of twilight, and its aftermath.
I'd like to have a staff, and issue out water wherever it fell.
Shahid: beloved in Persian, witness in Arabic, you drove back
No, not since yesterday    she taught me to be visible    then follow
Today I name the lasting roads:    artery    toll road   road of disguise
I took to the heat    like a dog to an electric fence    don't go past
And appreciated anew Italians' conceptions of love
The ice- and milt-stippled current; a sigh; the intervention of drift.
But moving forward as walking across it to get to the window
And wondering how she got out of the place she's usually in,
Calling up what never was in the tonal variations of gray
With a giant ribbon in my hair, the sorrow of living in time
Of fidelity and proximity, the latter is a watchword.
And flipping it forward and back in order to prove one theory
Deep in concentration, as I get closer I see she's just painting.
To be the type of person who goes to such places alone. It's her
Capitalizing proper nouns, stopping only to hurry forward,
I accept that her script is cuneiform and that a grave puzzle
Tall and undeniable, her back to us in the withering light.
From below an ear to the corner of a mouth, a smiley it's called,
Raised keloid tissue pale on darker skin. I remember as a kid
But it soon becomes apparent his wife and my husband are stewing,
But in the middle of my oysters I think of my great-grandfather,
To threaten that all my reading would destroy me, too, and I pictured
Bathsheba Everdene, was so rich and beautiful and stupid
Make an imperfect pomegranate-delivery system. It's tempting
To lie to the young. But you told us rightly about the beautiful
At last the animals starved by drought will eat the cactus, spines and all.
On an ice-cold morgue slab, smilingly shaking loose their beautiful hair.
It's hot hot in parts of Texas, but rain drowns Tennessee, people die.
You'd run into a hillbilly backwoods crazy, itching to kill you.
Bliss as sexual, and even after the curator mentioned sex
With all those lost spaces we try to recapture in our innocence
Then it goes away into the basement of old records that can't be
So much undone, or never done, or holding for a moment only,
So she'd be fresh she put on a little lipstick drawing on her smile
Apologize I let my sister succor those in need and suffer
The same jokes about eternity. New people join every day, of
Up ahead the kindly and the brutal alike keep disappearing.
And when she said she'd read a book about spontaneous combustion
A crime scene, reported by the driver on arrival in Phoenix
In the food co-op sighing as he shelves bags of Garden of Eatin'
We'd like to go on like this, Stan, but posthumously living uphill
From one life, and nearly reborn into the next, is taking its toll.
She had cut her toes on some glass in the high grass along the beach, too.
Rilke and George in hospital beds presumably, in Switzerland,
On the preexisting psychology). Exchange of equivalent gifts
Asserts a "rightness" in the universe�their word is an ascending
Invented especially for scenting land. Indeed, in one more day
With music and erotically bobbing hips. Is this the story
Of a soft-serve ice cream, and the family of seven laboring
And I put my arms around my son and told him, you have a future
I learned to see the world through the eye of a needle, to shape sticks,
To sing mourning songs. I thought myself the envy of the great teacher.
Then I believed for some years that humility was the great teacher.
One cannot know incompletion until completed. Newly in love,
We have plans for that house, for Nathan and for Cooper: today one draws,
The dancer-conqueror, and downs a slice of watermelon, red part
And the poison stars, exploding in their traces, cluster in the west.
And yet, Dear Ted, forgive me, but I need the boat of a bigger name:
Pinecones at the Villa Borghese: Fibonacci increments,
The Douglas-Fir can eschew standing in a fire which burns but does
Scrape the storm of its howl. Cellar as if to swarm, night as if to rot.
So archetypal in its split-level, clapboard ordinariness,
The sunlight gives the brick walls a ruddy, Hopper-esque gravitas?�
Self-abnegation of walking the streets of Natick, Massachusetts,
And there's a music of world-weary, self-extinguishing tragedy
Into a stiff-crested hoopoe with a pronged beak to replace his sword.
As we bear him, like an offering, towards the place I still call home,
One of them, as she loses her grip on the keel of that Cretan ship
Alice: the provenance, the gatekeeper. So it wasn't all hooey.
His second exoneural projecto-self in a pocket of subspace,
Photograph of two soldiers in camouflage battledress their slapdash
Of sun and sand and the faded brick pastels of walls roof-slates gables
Land of combat and they wary as cats ready for fight or flight and
Help the other back to base safe for one more day in which the basics
The moon snags the train of its wedding dress in the blackberry brambles.
The moon inhales the sluggish cloy of opium, exhales gypsum dust.
Water is only smooth like this on evenings of exceptional calm
It's a cup of no value, bought at a dry-goods-and-grocery shop
Honors the Holy Name. If you were exiled tomorrow, you would not
From gasoline comes internal combustion, comes a world of
Comes the pace of the century and its mode of transport and its
And afterwards he celebrates with a complimentary muskrat
And for this moment he is not worried about magneto coils or
It's picked up, throwing locutions like truly diabolical food
"You'll be a famous psychiatrist," I said to the academic.
Better to be the last chronicler of twilight, and its aftermath.
I'd like to have a staff, and issue out water wherever it fell.
Shahid: beloved in Persian, witness in Arabic, you drove back
No, not since yesterday    she taught me to be visible    then follow
Today I name the lasting roads:    artery    toll road   road of disguise
I took to the heat    like a dog to an electric fence    don't go past
And appreciated anew Italians' conceptions of love
The ice- and milt-stippled current; a sigh; the intervention of drift.
With a giant ribbon in my hair, the sorrow of living in time
Of fidelity and proximity, the latter is a watchword.
And flipping it forward and back in order to prove one theory
Deep in concentration, as I get closer I see she's just painting.
To be the type of person who goes to such places alone. It's her
Capitalizing proper nouns, stopping only to hurry forward,
I accept that her script is cuneiform and that a grave puzzle
Tall and undeniable, her back to us in the withering light.
From below an ear to the corner of a mouth, a smiley it's called,
Raised keloid tissue pale on darker skin. I remember as a kid
But it soon becomes apparent his wife and my husband are stewing,
But in the middle of my oysters I think of my great-grandfather,
To threaten that all my reading would destroy me, too, and I pictured
Bathsheba Everdene, was so rich and beautiful and stupid
Make an imperfect pomegranate-delivery system. It's tempting
To lie to the young. But you told us rightly about the beautiful
On an ice-cold morgue slab, smilingly shaking loose their beautiful hair.
It's hot hot in parts of Texas, but rain drowns Tennessee, people die.
You'd run into a hillbilly backwoods crazy, itching to kill you.
Bliss as sexual, and even after the curator mentioned sex
With all those lost spaces we try to recapture in our innocence
Then it goes away into the basement of old records that can't be
So much undone, or never done, or holding for a moment only,
So she'd be fresh she put on a little lipstick drawing on her smile
Apologize I let my sister succor those in need and suffer
The same jokes about eternity. New people join every day, of
Up ahead the kindly and the brutal alike keep disappearing.
And when she said she'd read a book about spontaneous combustion
A crime scene, reported by the driver on arrival in Phoenix
In the food co-op sighing as he shelves bags of Garden of Eatin'
We'd like to go on like this, Stan, but posthumously living uphill
From one life, and nearly reborn into the next, is taking its toll.
She had cut her toes on some glass in the high grass along the beach, too.
Rilke and George in hospital beds presumably, in Switzerland,
On the preexisting psychology). Exchange of equivalent gifts
Asserts a "rightness" in the universe�their word is an ascending
Invented especially for scenting land. Indeed, in one more day
With music and erotically bobbing hips. Is this the story
Of a soft-serve ice cream, and the family of seven laboring
And I put my arms around my son and told him, you have a future
I learned to see the world through the eye of a needle, to shape sticks,
To sing mourning songs. I thought myself the envy of the great teacher.
Then I believed for some years that humility was the great teacher.
One cannot know incompletion until completed. Newly in love,
We have plans for that house, for Nathan and for Cooper: today one draws,
The dancer-conqueror, and downs a slice of watermelon, red part
And the poison stars, exploding in their traces, cluster in the west.
And yet, Dear Ted, forgive me, but I need the boat of a bigger name:
The Douglas-Fir can eschew standing in a fire which burns but does
Scrape the storm of its howl. Cellar as if to swarm, night as if to rot.
So archetypal in its split-level, clapboard ordinariness,
The sunlight gives the brick walls a ruddy, Hopper-esque gravitas?�
Self-abnegation of walking the streets of Natick, Massachusetts,
And there's a music of world-weary, self-extinguishing tragedy
Into a stiff-crested hoopoe with a pronged beak to replace his sword.
As we bear him, like an offering, towards the place I still call home,
One of them, as she loses her grip on the keel of that Cretan ship
Alice: the provenance, the gatekeeper. So it wasn't all hooey.
His second exoneural projecto-self in a pocket of subspace,
Photograph of two soldiers in camouflage battledress their slapdash
Of sun and sand and the faded brick pastels of walls roof-slates gables
Land of combat and they wary as cats ready for fight or flight and
Help the other back to base safe for one more day in which the basics
The moon snags the train of its wedding dress in the blackberry brambles.
The moon inhales the sluggish cloy of opium, exhales gypsum dust.
Water is only smooth like this on evenings of exceptional calm
It's a cup of no value, bought at a dry-goods-and-grocery shop
Honors the Holy Name. If you were exiled tomorrow, you would not
From gasoline comes internal combustion, comes a world of
Comes the pace of the century and its mode of transport and its
And afterwards he celebrates with a complimentary muskrat
And for this moment he is not worried about magneto coils or
It's picked up, throwing locutions like truly diabolical food
"You'll be a famous psychiatrist," I said to the academic.
Better to be the last chronicler of twilight, and its aftermath.
I'd like to have a staff, and issue out water wherever it fell.
