#!/usr/bin/env python

import time
import shelve
import sys 
from snd_tui.lib import init_curse, end_curse, shelf_put, shelf_get, shelf_clear
from snd_tui.lib import log_exception, get_syll_count, get_syll_lists, Menu
from snd_tui.lib import get_cmu_wbs, get_top_wbs, get_ra_wbs
from snd_tui.cli import print_screen, do_cli
from snd_tui.welcome import do_welcome
from snd_tui.song import Song
from snd_conf import logger, Globals
import nltk
from nltk.corpus import cmudict

logger.info("Loading dictionaries...")
start_loading=time.time()
try:
    start = time.clock()
    # dictionary used for syllable count  
    Globals.CMUDICT = cmudict.dict()
    Globals.CMUDICT_WORDS = tuple(cmudict.words())
    logger.debug("CMUDICT loaded in: {}s".format(time.clock()-start))
except LookupError:
    logger.debug("NLTK cmudict package not found.")
    logger.debug("Downloading to {}...".format(
            nltk.downloader.Downloader().default_download_dir()))
    if nltk.download('cmudict'):
        logger.debug("Done.")
        from nltk.corpus import cmudict
        Globals.CMUDICT = cmudict.dict()
    else:
        logger.debug("ERROR: Failed to download cmudict...")
        Globals.CMUDICT = dict()
# get words lists for random word generation
# words in cmudict by syllcount
get_cmu_wbs()
logger.info("CMUDICT done.")
# words in top5000 dict by syllconut
get_top_wbs()
logger.info("TOP5000 done.")
logger.debug("Dict loading took: {} secs".format(time.time() - start_loading))



def do_new():
    Globals.START_TIME = time.time()
    sys.stdout.write("\x1b]2;SND phase 0\x07")
    
    # words in RA files
    get_ra_wbs()
#    for t in Globals.RA_WBS:
#        logger.debug("{} sylls -> tuple len: {} -> set len: {}".format(Globals.RA_WBS.index(t),
#                                                                       len(t), 
#                                                                       len(set(t))))
    # do cli interface
    while True:
        if Globals.START_NEW:
            initial_config = do_welcome(Globals.STDSCR)
            logger.info("Initial Config: \n{}".format(initial_config))
            Globals.SONG = Song(initial_config)
            Globals.START_NEW = False
        print_screen(Globals.STDSCR)
        if not do_cli(Globals.STDSCR):
            Globals.STDSCR.clear()
            # when returning False, start new song (when selecting "New" option 
            #    from menu); otherwise the same SONG instance will be shown. 
            Globals.START_NEW = True 
            break

def not_implemented():
    Globals.STDSCR.addstr(10, 35, "Not implemented yet...")                    


if __name__ == '__main__':
    log_delimiter = "#"*20 + time.strftime("%a, %d %b %Y %X +0000", 
                                           time.gmtime()) + "#"*10
    logger.debug("\n"*2 + log_delimiter + "\n") 
    for line in Globals.PRE_LOG:
        logger.debug(line)

    shelf_clear()
    logger.info("Let's write some rhimes...")
    try:
        Globals.STDSCR = init_curse()

        submenu_items = [
            ('New Album', not_implemented),
            ('Edit Albums', not_implemented)
            ]
        submenu = Menu(submenu_items, Globals.STDSCR, 'Back', 'Albums')
    
        main_menu_items = [
            ('New Song', do_new),
            ('Load Song', not_implemented),
            ('Albums', submenu.display),
            ]                                                            
        main_menu = Menu(main_menu_items, Globals.STDSCR, title='SND Main Menu')                       
        main_menu.display()                                          
        
    except Exception as e:
        # exit curses and print trace to log and terminal
        log_exception(e, "Unknown exception caught in main file:")
        if Globals.STDSCR:
            end_curse(Globals.STDSCR)
        logger.info("""
\t!!! WARNING: SND might have crashed. Please check log for details.
""")
        

    finally:
        # handle key interrupts and normal program exit    
        if Globals.STDSCR:
            end_curse(Globals.STDSCR)



