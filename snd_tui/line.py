from snd_tui.lib import *
from snd_conf import logger, Globals



class Word(object):

    def __init__(self, value='', is_locked=False):
        """
        :type value: str
        :param value: actual word
        :type is_locked: boolean
        :param is_locked: True or False
        """
        self.value = value
        self.is_locked = is_locked
        self.rlink = 0
        self.rlink_anchor = 0

    def get_syll_count(self):
        return get_syll_count(self.value)

    def set_rlink(self, rlink):
        self.rlink = rlink

    def unset_rlink(self):
        self.rlink = 0

    def set_value(self, new_val):
        self.value = new_val
        
    def get_value(self):
        return self.value

    def lock(self):
        self.is_locked = True

    def unlock(self):
        self.is_locked = False

    def set_rlink_anchor(self):
        self.rlink_anchor = 1

    def unset_rlink_anchor(self):
        self.rlink_anchor = 0

    def __str__(self):
        return "{} {}".format(self.value, self.is_locked)



class Line(object):

    def __init__(self, syllables=0, word_count=0, enabled=True, 
                 content='', index = -1):
        self.word_count = word_count
        self.required_syll_count = int(syllables)
        self.is_enabled = enabled # X marks the spot- True=line will be affected by randomization
        self.is_locked = False # True will not be affected by randomization
        if content == '':
            self.content = 13 * "... "
        else:
            self.content = content
        self.index = index # line index inside section (-1 = index unavailable) 
        self.actual_syll_count = 0
        self.words = [] # list of Word objects; will be filled at first ra 
        self.has_locked_words = False
        self.rlink = 0
        self.rlink_word_idx = -1 # last word will be rlinked by default
        self.rlink_anchor = 0


    def __eq__(self, other):
        if(self.word_count == other.word_count
           and self.required_syll_count == other.required_syll_count
           and self.is_enabled == other.is_enabled
           and self.is_locked == other.is_locked
           and self.content == other.content
           and self.actual_syll_count == other.actual_syll_count
           and self.has_locked_words == other.has_locked_words):
            return True
        return False


    def __ne__(self, other):
        if(self.word_count != other.word_count
           or self.required_syll_count != other.required_syll_count
           or self.is_enabled != other.is_enabled
           or self.is_locked == other.is_locked
           or self.content != other.content
           or self.actual_syll_count != other.actual_syll_count
           or self.has_locked_words != other.has_locked_words):
            return True
        return False
    

    def set_index(self, index):
        self.index = index


    def clear(self):
        self.set_content(13 * "... ")
        

    def randomize(self, words_per_line=''):
        """
        Randomize line by selecting a random line from the apropriate
        randomize file from Globals.RA_PATH.

        :type words_per_line: int
        :param words_per_line:Can be:
                                 ''   - (empy string) will be ignorred
                                 '+'  - increment number of words for each line
                                 '-'  - decrement number of words for each line
        :rtype: (int, str)
        :return: Tuple containing int code and detailed message.
              0 = OK
              1 = Line locked, not randomized
             -1 = Exception 
        """
        old_word_count = self.word_count
        message = ''
        code = 0

        if self.is_locked:
            message = "Line {} is locked. Will not randomize.".format(self.index+1)
            logger.debug(message)
            return (1, message)

        if words_per_line:
            if words_per_line == '+':
                self.word_count += 1
                message = "WARNING: Unable to find desired syllable count with more words."
            elif words_per_line == '-':
                if self.word_count > 1:
                    self.word_count -= 1
                    message = "WARNING: Unable to find desired syllable count with less words."
                else:
                    logger.debug("Could not decrement word count. It has value: {}.".format(word_count))
        try:
            if self.is_enabled:
                if self.has_locked_words:
                    logger.debug("Content: {}".format(self.get_content()))
                    for word in self.words:
                        logger.debug("'{}' is locked - {}".format(word.value, 
                                                                word.is_locked))
                        if word.is_locked:
                            continue
                        else:
                            word_syll_count = min(get_syll_count(word.value))
                            word.set_value(get_random_word(word_syll_count, 
                                                           rmode=Globals.RMODE))
                            logger.debug("New word: '{}' - locked: {}".format(word.value, 
                                                                              word.is_locked))
                    self.update_content()
                    self.update_syll_count()
                else:
                    ra_file_name = '{}syllable.txt'.format(self.required_syll_count)
                    ra_file = os.path.join(Globals.RA_PATH, ra_file_name)
                    logger.debug("RA file: {}".format(ra_file))
                    new_content = get_random_line(ra_file, self.word_count)
                    if get_word_count(new_content) != self.word_count\
                            and words_per_line in ['+', '-']:
                        new_content = get_random_line(ra_file, old_word_count)
                        if words_per_line == '+':
                            message = "WARNING: Unable to find desired syllable count with more words."
                        elif words_per_line == '-':
                            message = "WARNING: Unable to find desired syllable count with less words."
                        self.word_count = old_word_count
                    else:
                        message = "Randomized line {}.".format(self.index+1)
                    self.set_content(new_content)
#                    logger.debug("Randomized line {} to '{}'".format(self.index, 
#                                                                     self.get_content()))
                    # keep word rlink info
                    if self.rlink != 0:
                        self.words[self.rlink_word_idx].set_rlink(self.rlink)
                        if self.rlink_anchor == 1:
                            self.words[self.rlink_word_idx].set_rlink_anchor()
            else:
                message = "Line {} is disabled. Will not randomize.".format(self.index+1)
                code = 1
                logger.debug(message)
        except IOError as e:
            message = str(e)
            code = -1
            log_exception(e)
        except Exception as e:
            message = str(e) 
            code = -1
            log_exception(e)
            
        return (code, message)


    def replace(self, from_text, to_text):
        """
        Replace from_text with to_text in the line contents.
        """
        new_content = self.get_content().replace(from_text, to_text)
        message = self.set_content(new_content)
        if message:
            logger.debug("""{}
While trying to replace: 
\t '{}'
\t with '{}' 
\t in line '{}'""".format(message, from_text, to_text, self.get_content()))
            return message + "\n\tCheck log for details."


    def disable(self):
        self.is_enabled = False


    def enable(self):
        self.is_enabled = True


    def show(self, window, y, x):
        """
        Print the line in the specified window on the line y starting 
        from position x.

        :type window: curses window
        :param window: a curses window (obtained with curses.initscr())
                       the window coordinates where to start printing
        :type y: int
        :type x: int
        """
        window.addstr(y, x, self.get_content())
        window.refresh()
    
    def get_content(self):
        self.update_content()
        return self.content

    def set_content(self, text):
        message = ''
        if len(text) <= Globals.MAX_LINE_CHARS:
            logger.debug("OLD content: '{}'".format(self.get_content()))
            text = text.capitalize()
            if not self.has_locked_words and not self.is_locked:
                self.words = []
                for word in tokenize(text):
                    self.words.append(Word(word, False))
                    self.content = ' '.join([word.get_value() for word in self.words]) 
                self.update_syll_count()
                self.update_word_count()
            else:
                message = "ERROR: Line is locked or has locked words."
            logger.debug("NEW content: '{}'".format(self.get_content()))
        else:
            message = "ERROR: Substitute not performed as the new text is too long for the line."

        return message


    def get_syll_count(self):
        return get_text_syll_count(self.get_content())

    def update_syll_count(self):
        self.actual_syll_count = self.get_syll_count()

    def update_word_count(self):
        self.word_count = get_word_count(self.get_content())

    def lock(self):
        self.is_locked = True
    
    def unlock(self):
        self.is_locked = False
        if self.has_locked_words:
            [word.unlock() for word in self.words if word.is_locked]
    
    def lock_words(self, lock_range):
        try:
            lock_range_l = lock_range.split('-')
            s_idx = int(lock_range_l[0]) - 1
            if not check_positive_int(s_idx, self.word_count, min_limit=-1):
                return -1
            f_idx = int(lock_range_l[1])
            for word in self.words[s_idx:f_idx]:
                word.lock()
        except IndexError:
            self.words[s_idx].lock()
        except ValueError:
            modifier = lock_range[-1:]
            word_idx = lock_range[:-1]
            if not check_positive_int(word_idx, self.word_count, min_limit=-1):
                return -1
            word_idx = int(word_idx)
            if modifier == '+':
                word_range = self.words[word_idx-1:]
            elif modifier == "-":
                word_range = self.words[:word_idx]
            else:
                return -1
            for word in word_range:
                word.lock()
        self.has_locked_words = True    
        return 0

    def unlock_words(self, unlock_range):
        if self.is_locked:
            self.is_locked = False
            if self.words:
                for word in self.words:
                    word.lock()
            else:
                self.words = [Word(word, True) for word in tokenize(self.get_content())]
        try:
            unlock_range_l = unlock_range.split('-')
            s_idx = int(unlock_range_l[0]) - 1
            if not check_positive_int(s_idx, self.word_count, min_limit=-1):
                return -1
            f_idx = int(unlock_range_l[1])        
            for word in self.words[s_idx:f_idx]:
                word.unlock()
        except IndexError:
            self.words[s_idx].unlock()
        except ValueError:
            modifier = unlock_range[-1:]
            word_idx = unlock_range[:-1]
            if not check_positive_int(word_idx, self.word_count, min_limit=-1):
                return -1
            word_idx = int(word_idx)
            if modifier == '+':
                word_range = self.words[word_idx-1:]
            elif modifier == "-":
                word_range = self.words[:word_idx]
            else:
                return -1
            for word in word_range:
                word.unlock()
        self.has_locked_words = False
        if len([word for word in self.words if word.is_locked]) > 0:
            self.has_locked_words = True    
        return 0

    def set_rlink(self, rlink, word_index):
        logger.debug("line={}, rlink={}, word_index={}".format(self.index, rlink, word_index))
        if self.rlink != 0:
            return (-1, "Line {} alredy rlinked".format(self.index+1))
        self.rlink = rlink
        if self.words:
            if word_index == '':
                word_index = -1
            self.words[word_index].set_rlink(rlink)
            self.rlink_word_idx = word_index
            logger.debug("set rlink on word {}: {}".format(
                    word_index, self.words[word_index].get_value()))
        return(0, "Rlink added on line {}:{}".format(self.index+1, word_index))
    
    def unset_rlink(self, word_index):
        self.rlink = 0
        if self.words:
            if word_index != '':
                self.words[word_index].unset_rlink()
                logger.debug("removed rlink on line {} word {}:".format(
                        self.index, word_index))
            else:
                logger.debug("Unsetting all words.")
                for word in self.words:
                    word.unset_rlink()        
        return(0, "Rlink removed from line {}".format(self.index+1))
        
    def set_rlink_anchor(self):
        self.rlink_anchor = 1
        for word in self.words:
            if word.rlink != 0:
                word.set_rlink_anchor()

    def unset_rlink_anchor(self):
        self.rlink_anchor = 0
        for word in self.words:
            word.unset_rlink_anchor()

        
    def get_progress(self):
        locked_sylls = 0
        unlocked_sylls = 0
        line_syll_count = self.get_syll_count()
        if self.is_locked:
            locked_sylls = line_syll_count
        elif self.has_locked_words:
            for word in self.words:
                syll_count = max(word.get_syll_count())
                if word.is_locked:
                    locked_sylls += syll_count
                else:
                    unlocked_sylls += syll_count
        else:    
            unlocked_sylls = line_syll_count
        return locked_sylls, unlocked_sylls

    def update_content(self):
        if self.words:
            self.content = ' '.join([word.get_value() for word in self.words if word])

    def set_word_value(self, word_index, word_value):
        self.words[int(word_index)].set_value(word_value)
        self.update_content()




