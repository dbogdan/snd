import textwrap
from snd_conf import Globals


class CmdHelp(object):

    def __init__(self, section, command, message):
        self.section = section
        self.command = command
        self.message = message


class Manual(object):

    def __init__(self):
        self.run_help_message = "\n\tUse '?'or 'help' for a list of accepted commands."
        self.entries = []

    def add_entry(self, cmd_help):
        """
        Append command help instance to the manual entries

        :type cmd_help: CmdHelp instance
        """
        self.entries.append(cmd_help)

    def get_help(self, cmd):
        for entry in self.entries:
            if cmd == entry.command:
                return (0, entry.message)
        return (-1, "ERROR: Command '{}' not found.{}".format(cmd, self.run_help_message))

    def get_sections(self):
        """
        Get a list of all sections
        """
        return set([help_entry.section for help_entry in self.entries])

    def get_section_summary(self, section):
        """
        Get a list of all commands in the specified section

        :rtype: (int, str)
        :return: Returns a tuple representing and exit value and user message
             Exit values can be:
                   -1 - specified section does not exist
                    0 - all OK
        """
        sections = self.get_sections()
        if section not in sections:
            return (-1,"ERROR: Section '{}' not found.{}".format(section,
                                                                 self.run_help_message))
        section_cmds = [entry.command for entry in self.entries 
                        if entry.section == section.lower()]
        message = "{:<{max_len}}: {}".format(section.capitalize(), 
                                       ', '.join(section_cmds),
                                       max_len=len(max(sections, key=len)))
        return (0, message)

    def get_all_summary(self):
        """
        Get a list of all commands grouped by section
        """
        summary = "Command summary:\n"
        for section in self.get_sections():
            ret_code, usr_mess = self.get_section_summary(section)
            summary += "\t* {}\n".format(usr_mess)
        return summary
        
    
man = Manual()


###################
# Add help entries
#


# system 

for key in ['exit', 'quit', 'q']:
    man.add_entry(
        CmdHelp( 
            section = 'system',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - exit SND      
Usage:       {0}  
Description: Exit program'''.format(key)
            )
        )
for key in ['help', '?']: 
    man.add_entry(
        CmdHelp(
            section = 'system',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - Help       
Usage:       {0} [command]     
Description: Show command help. 
             If no argument given, shows complete command list'''.format(key)
            )
        )
man.add_entry(
    CmdHelp(
        section = 'system',
        command = 'limits'.format(key),
        message = '''[Help limits]
Name:        limits       
Usage:       limits      
Description: Show section adn lines limits.'''
        )
    )
for key in ['ls', 'linespace']:
    man.add_entry(
        CmdHelp(
            section = 'system',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - linespace       
Usage:       {0}   
Description: Toggle printing empty line between section lines.'''.format(key)
            )
        )

    
########
# song #
########
man.add_entry(
    CmdHelp(
        section = 'song',
        command = 'new',
        message = '''[Help new]
Name:        new
Usage:       new
Description: Start a new song.
             Uppon confirmation, discards all unsaved data of current song.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'song',
        command = 'title',
        message = '''[Help title]
Name:        title 
Usage:       title New_song_title
Description: Change title of song. 
             Argument is mandatory.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'song',
        command = 'go',
        message = '''[Help go]
Name:        go - preview entire song lyrics in one screen 
Usage:       go
Description: Preview entire song lyrics in one screen

             Use Up, Down, PgUp, PgDn, Home or End to scroll up and down.
             Hit 'q' to exit.'''
        )
    )
for key in ['z', 'undo']: 
    man.add_entry(
        CmdHelp(
            section = 'song',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - Undo       
Usage:       {0}      
Description: Undo the last action.'''.format(key)
            )
        )
for key in ['.', 'redo']: 
    man.add_entry(
        CmdHelp(
            section = 'song',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - Redo       
Usage:       {0}      
Description: Cancel last undo action.'''.format(key)
            )
        )
man.add_entry(
    CmdHelp(
        section = 'song',
        command = 'ln',
        message = '''[Help ln]
Name:        ln - line numbers
Usage:       ln
Description: Show/hide line numbers. '''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'song',
        command = 'save',
        message = '''[Help save]
Name:        save
Usage:       save file_name
Description: Save the song to specified file.

             file_name - mandatory argument.
                Can be a simple file name, or an absolute or relative path to a 
                file name.
             Save file will be overwritten if already exists.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'song',
        command = 'export',
        message = '''[Help export]
Name:        export - save song as plain text
Usage:       export file_name
Description: Save the song as plain text in the specified file

             file_name - mandatory argument.
                Can be a simple file name, or an absolute or relative path to a 
                file name. 
             Save file will be overwritten if already exists.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'song',
        command = 'load',
        message = '''[Help load]
Name:        load
Usage:       load file_name
Description: Load the song from specified file.

             file_name - mandatory argument.
                Can be a simple file name, or an absolute or relative path to a 
                file name.
                MUST be a file resulted from 'save' command'''
        )
    )
for key in ['wordlist', 'wl']:
    man.add_entry(
        CmdHelp( 
            section = 'song',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - set custom 'ra' path
Usage:       {0} dir
Description: Read syllable files from the 'dir' directory.
             'dir' can be an absolute path or relative path 
                to the project dir
'''.format(key)
            )
        )

for key in ['show', 'sh']:
    man.add_entry(
        CmdHelp( 
            section = 'song',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - show various parameters      
Usage:       {0} r|p|wl|rm 
Description: Show various background settings:
                  - 'rlinks' or 'r'    - show coloured rhyme links.
                  - 'prog' or 'p'      - show progress. 
                  - 'wordlist' or 'wl' - show word list stats.
                  - 'rmode' or 'rm'    - show randomize mode (rmode)
'''.format(key)
            )
        )
for key in ['hide', 'h']:
    man.add_entry(
        CmdHelp( 
            section = 'song',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - hide various parameters      
Usage:       {0} rlinks 
Description: Hide various background settings:
                  - rlinks - hide coloured rhyme links.
                 (...more to be added...)
'''.format(key)
            )
        )


########
# line #
########
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'l',
        message = '''[Help l]
Name:        l - lock
Usage:       l line_no [word_range]
Description: Lock specific lyrics/words.

             Lock a line or a range of words in a line. 
             For example:
                  l 3 1-4  lock the range in line 3 from words 1 to 4
                  l 3      lock entire line 3
                  l 3 2+   lock from word 2 to end of line
                  l 3 2-   lock from word 2 to beginning of line.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'u',
        message = '''[Help u]
Name:        u - unlock
Usage:       u line_no [word_range]
Description: Unlock specific lyrics/words.

             Unlock a line or a range of words that have been 
              previously locked.
             For example:
                  u 3 1-4  unlock the range in line 3 from words 1 to 4
                  u 3      unlock entire line 3
                  u 3 2+   unlock from word 2 to end of line
                  u 3 2-   unlock from word 2 to beginning of line.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 's',
        message = '''[Help s]
Name:        s - Substitite text
Usage:       s/from/to[/g]
Description: Similar to 'vi'.
             
             /g on the end makes the replace do a global (entire song) 
             all sections replacement. 
             
             For example:
             * to replace all instances in the current (on screen) 
               section of "me" to "you" the following command would be used: 
                    s/me/you
             * to globally (entire song) replace all instances of "me" to "you" 
               the following command would be used: 
                    s/me/you/g'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'ael',
        message = '''[Help ael]
Name:        ael - add empty line 
Usage:       ael position
Description: Same as "al" but with no automatic randomization occurring, 
             just a blank line showing.
             
             The required syllables for the line is taken from the same value 
             as the previous line (the line immediately above it).
             The parameter ('position') is mandatory.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'al',
        message = '''[Help al]
Name:        al - add line 
Usage:       al position
Description: Add a line with random content on the specified position in the
             active section
             
             The required syllables for the line is taken from the same value 
             as the previous line (the line immediately above it).
             The parameter ('position') is mandatory.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'dl',
        message = '''[Help dl]
Name:        dl - delete line
Usage:       dl line_number
Description: Delete the line specified by line_number.
             The argument is mandatory. '''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'rl',
        message = '''[Help rl]
Name:        rl - randomize line
Usage:       rl [line_number]
Description: Randomize the line specified by line_number.

             If line number is not specified, a random line  will be selected.
             If the line is in an 'OFF[ ]' state, it will not be randomized.
             Only unlocked words on the line will be randomized.
             '''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'cl',
        message = '''[Help cl]
Name:        cl - copy line
Usage:       cl line_number
Description: Copy the line specified by line_number.
             The argument is mandatory. '''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'pl',
        message = '''[Help pl]
Name:        pl - paste line
Usage:       pl line_number
Description: Paste a line previousely copied with "cl"'''
        )
    )
for key in ['c', 'clear']:
    man.add_entry(
        CmdHelp(
            section = 'line',
            command = '{}'.format(key),
            message = '''[Help {0}]
Name:        {0} - clear line
Usage:       {0} line_number|*
Description: Clear a specific specific or all lines in current section.
             
             E.g. '{0} 1' - clear line 1
                  '{0} *' - clear all lines in current section'''.format(key)
            )
        )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'e',
        message = '''[Help e]
Name:        e - edit line
Usage:       e line_no    
Description: Start line ediditng mode. A line number must be specified.
             Specifying just a line number will edit that line.
             E.g. 'e 4' edit mode with line 4 selected
                  '5'   edit mode with line 5 selected

             Keys (asside from common Emacs bindings):
                Up/Down    - Edit line above or below
                Left/Right - Move trhough line text
                Enter      - exit and apply new changes
                Esc        - exit and discard changes'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'lo',
        message = '''[Help lo]
Name:        lo - lines state on/off 
Usage:       lo line_identifier [state] 
Description: Turns the [ ] or [X] ON/OFF state for one or more lines.

             E.g. 'lo 1'     will toggle state for line 1
                  'lo 1 2 3' will toggle state for lines 1, 2, 3
                  'lo * x'   turns OFF all the lines in the current section
                  'lo *'     turns ON all the lines in the current section
                  '''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'ml',
        message = '''[Help ml]
Name:        ml - move line
Usage:       ml pos[+|-]
Description: Moves a line either up or down. 
             E.g. 'ml 4+' Will move line 4 upwards (to position 5).'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = 'slg',
        message = '''[Help slg]
Name:        slg - syllable line guide
Usage:       slg line_no syllables_no
Description: Change the syllable guide number for a specific line.
             
             Both arguments are mandatory and must be int.
             Examples:
                slg 1 15 - change the syllables required value for line 1 to 15.
                slg * 7 - sets the syllable required value for all lines in 
                          the current section to 7'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = '+',
        message = '''[Help +]
Name: + - randomize more
Usage: + 
Description: Repeat the last single line randomization with the same number of 
             syllables but with more number of words (results in more words but
             shorter words).'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'line',
        command = '-',
        message = '''[Help -]
Name: - - randomize less
Usage: - 
Description: Repeat the last single line randomization with the same number of 
             syllables but with less number of words (results in less words but
             longer words).'''
        )
    )


###########
# section #
###########
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'rr',
        message = '''[Help rr]
Name: rr - random random
Usage: rr 
Description: Performs random instance of a randomization command
             (ra, rl or jl).'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'rmode',
        message = '''[Help rmode]
Name: rmode - randomize mode
Usage: rmode 1|2|3 
Description: rmode 1 - uses dictionary (like it does currently)
             rmode 2 - use only syllable files
             rmode 3 - does a 50% dictionary and 50% syllable files propagation
                       of required syllables.

             Of course rmode is only taken in to effect when partial lines are 
             needed to be populated. which is fine, that is because when it first 
             pulls required syllables it gets them from the syllable files.

Example:

   rmode 1
   ra
   l 1 1-3

   let's say the line 1 now needs 10 syllables...

   when rmode = 1 then pull required words from dictionaries only.
   when rmode = 2 then pull required words from syllable files only.
   when rmode = 3 then on the first instance pull required words from syllable files 
                then if another ra on line 1 is performed then dictionary (like it 
                does now) word pull. 
                A round robin dict, syllable files, dict, syllable files, dict,  etc.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'ra',
        message = '''[Help ra]
Name: ra - randomize all
Usage: ra 
Description: This command will randomize anything on the current section 
                 (the section displayed on the screen - and not the other 
                 sections in the back ground). It will only randomize areas 
                 that are unlockedand with a line state of 'ON/[x]'.
             If the a line state is 'OFF/[ ]' then randomization will not affect
                 this line.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'jl',
        message = '''[Help jl]
Name:        jl - jumble lines 
Usage:       jl [a|s]
Description: "jl" randomises the lines in the current section
             "jl s" randomises the lines within the same section 
                   (this should jumble all the lines of verses for example 
                   so a line from another verse would/could appear on the 
                   active section)
             "jl a" randomises ALL the lines of the entire song.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'n',
        message = '''[Help n]
Name:        n - next section        
Usage:       'n' or right arrow key   
Description: Jumps to the next section. 
             If at the end, a message will be returned stating: 
             "You are at the end of the song".'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'b',
        message = '''[Help b]
Name:        b - previous section        
Usage:       b or left arrow key   
Description: Jumps to the previous section. 
             If at the beginning, a message will be returned stating: 
             "You are at the beginning of the song".'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'as',
        message = '''[Help as]
Name:        as - add section
Usage:       as new_section_type
Description: Adds a new section after current section. Or a new current section 
             if all sections were deleted.

             new_section_type - letter describing section type.
             Accepted values: 
                 'V' or 'v' = Verse, 'C' or 'c' = Chorus, 
                 'B' or 'b' = Bridge, 'M' or 'm' = Middle 8.
             The argument is mandatory. '''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'ds',
        message = '''[Help ds]
Name:        ds - delete section
Usage:       ds 
Description: Deletes the active section.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'cs',
        message = '''[Help cs]
Name:        cs - copy section
Usage:       cs 
Description: Copies the active section.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'ps',
        message = '''[Help ps]
Name:        ps - paste section
Usage:       ps 
Description: Paste previousely copied section after active section.'''
        )
    )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'ms',
        message = '''[Help ms]
Name:        ms - move current section
Usage:       ms +|-
Description: Moves current section either up or down. 
             E.g. 'ms +' -> moves right/down the sections 
                  'ms -' -> moves left/up the sections.'''
        )
    )
for key in ['r', 'rlink']:
    man.add_entry(
        CmdHelp(
            section = 'section',
            command = '{0}'.format(key),
            message = '''[Help {0}]
Name:        {0} - Create/delete rhyme links
Usage:       {0} c|d line1 line2 ... lineN
Description: Creates or deletes a rhyme link between specified lines.
             The lines are specified in any order by their line numbers 
             (as shown by the 'ln' command) separated by space.
 
             Eg. {0} c 7 1 3 - creates a link between lines 1, 3 and 7
                               the word on line 1 will be the anchor
                 {0} c 1:5 3 - creates a link between line 1 word 4, and
                               last word on line 3
                 {0} d 12 5  - deletes the rlink(s) of lines 5 and 12 and 
                               of any other lines rlinked with them.
'''.format(key)
            )
        )
man.add_entry(
    CmdHelp(
        section = 'section',
        command = 'make',
        message = '''[Help make]
Name:        make 
Usage:       make new_section_type[+]
Description: Change the current section designator.

             new_section_type - mandatory- letter describing section type.
             + - optional - if specified, will overvrite all sections of type 
                  "new_section_type" with the current section
             Accepted values: 
                 'V' or 'v' = Verse, 'C' or 'c' = Chorus, 
                 'B' or 'b' = Bridge, 'M' or 'm' = Middle 8.
             
             Eg. make C  - make the current section in to a Chorus section
                 make C+ - make the current section in to a Chorus section and 
                           overwrite all other Chorus sections with the current section.'''

        )
    )

