import random
from snd_tui.lib import *
from snd_conf import logger
from snd_conf import Globals

WIN_TITLE = "WELCOME - New Song"

def show_clear_welcome(window):
    window.clear()
    window.border(0)
    window.addstr(0, 5, WIN_TITLE, curses.A_UNDERLINE)
    window.refresh()

def get_song_name(window):
    return get_text(window, 
                    "Enter song title: ", 
                    "(You can leave this blank. Can be changed later)", 
                    Globals.INPUT_Y, Globals.INPUT_X,
                    Globals.MAX_TITLE_CHARS)
        
def get_song_structure(window):
    sstr = get_text(
        window,
        "Enter song structure: ",
        """For example: 
\tVVCVCMCC \t\t\tKey:
\tVBCVBCMCC\t\t\tV = Verse \tC = Chorus     
\tVCVCVC   \t\t\tB = Bridge\tM = Middle 

\t Maximum sections allowed: {}.

Hit 'enter' for default random choice from:
\t {}
""".format(Globals.MAX_STRUCTURE_CHARS, Globals.DEFAULTS['structure']),
        Globals.INPUT_Y,
        Globals.INPUT_X,
        Globals.MAX_STRUCTURE_CHARS)
    return sstr.upper()

def check_song_structure(structure):
    if structure:
        for ch in structure:
            if ch not in Globals.VALID_SECTION_INITIALS:
                return False
            if len(structure) > Globals.MAX_STRUCTURE_CHARS:
                return False
        return True
    else:
        return False

def get_lines_no(window, section):
    return get_text(window,
                    "Number of lines for the \"{}\" (default {}): ".format(section, 
                                                                           Globals.DEFAULTS[section+"_lines"]),
                    "\tMaximum value:{}".format(Globals.MAX_LINES), 
                    Globals.INPUT_Y, 
                    Globals.INPUT_X,
                    len(str(Globals.MAX_LINES)))

def get_syllables_no (window, section):
    return get_text(window,
                    "Number of syllables for each {} line (default {}): ".format(section, 
                                                                                  Globals.DEFAULTS[section+"_syllables"]),
                    "\tMaximum value:{}".format(Globals.MAX_SYLLABLES),
                    Globals.INPUT_Y, 
                    Globals.INPUT_X,
                    len(str(Globals.MAX_SYLLABLES)))

def get_section_specs(window, section):
    """ 
    section =  verse, bridge, chorus or middle 8
    returns tuple:
         (lines_no, syllables_per_line)
    """  
 
    error_message_lines = "ERROR: Invalid value. Please enter a positive, smaller than {}, integer.".format(Globals.MAX_LINES+1)
    error_message_sylls = "ERROR: Invalid value. Please enter a positive, smaller than {}, integer.".format(Globals.MAX_SYLLABLES+1)

    # lines
    show_clear_welcome(window)
    section_lines_no = get_lines_no(window, section)
    if not section_lines_no:
        section_lines_no = Globals.DEFAULTS[section+"_lines"]
    while not check_positive_int(section_lines_no, Globals.MAX_LINES+1):
        window.addstr(Globals.ERROR_Y, Globals.INPUT_X, error_message_lines)
        section_lines_no = get_lines_no(window, section)
        if not section_lines_no:
            section_lines_no = Globals.DEFAULTS[section+"_lines"]
    clear_line(window, Globals.ERROR_Y)

    # syllables
    show_clear_welcome(window)
    section_syllables_no = get_syllables_no(window, section)
    if not section_syllables_no:
        section_syllables_no = Globals.DEFAULTS[section+"_syllables"]
    while not check_positive_int(section_syllables_no, Globals.MAX_SYLLABLES+1):
        window.addstr(Globals.ERROR_Y, Globals.INPUT_X, error_message_sylls)
        section_syllables_no = get_syllables_no(window, section)
        if not section_syllables_no:
            section_syllables_no = Globals.DEFAULTS[section+"_syllables"]
    clear_line(window, Globals.ERROR_Y)
    return (section_lines_no, section_syllables_no)


def do_welcome(window):
    song_info = {}

    # title
    show_clear_welcome(window)
    song_name = get_song_name(window)
    logger.debug("initial name: '{}'".format(song_name))
    if not song_name:
        song_name = Globals.DEFAULTS['name']
    else:
        while len(song_name) > Globals.MAX_TITLE_CHARS:
            window.addstr(Globals.ERROR_Y, Globals.INPUT_X, 
                          "ERROR: Song title must be maximum {} chars in length".format(
                    Globals.MAX_TITLE_CHARS))
            song_name = get_song_name(window)
            
    song_info['name']= song_name

    # structure
    show_clear_welcome(window)
    song_structure = get_song_structure(window)
    while not check_song_structure(song_structure):
        if not song_structure:
            song_structure = random.choice(Globals.DEFAULTS['structure'])
            break
        window.addstr(Globals.ERROR_Y, Globals.INPUT_X, 
                      "ERROR: Invalid song structure entered!!! Please try again")
        song_structure = get_song_structure(window)

    song_info['structure'] = song_structure        

    # sections
    for section_initial in Globals.VALID_SECTION_INITIALS:
        if section_initial in set(song_structure):
            section_name = get_section_from_initial(section_initial)

            song_info[section_name+'_specs'] = get_section_specs(window, section_name)

    return song_info
