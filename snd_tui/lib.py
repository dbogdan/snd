import curses                                                                
from curses import panel                                                     
import curses.ascii
import shelve
import os
import random
import string
import nltk
import sys, time
import curses.textpad as textpad
from snd_conf import logger
from snd_conf import Globals
import re
from re import match, search
import textwrap


class timer:
    def __init__(self, func):
        self.func = func
        self.alltime = 0
    def __call__(self, *args, **kargs):
        start = time.clock()
        result = self.func(*args, **kargs)
        elapsed = time.clock() - start
        self.alltime += elapsed
        pargs = []
        for arg in args:
            parg = arg
            if len(str(arg)) > 10:
                parg = str(arg)[:10]
            pargs.append(parg)
        logger.debug('%s %s: %.5fs, %.5fs' % (self.func.__name__, pargs, 
                                              elapsed, self.alltime))
        return result


class Re(object):
    """
    Enables cascading through multiple regex with if, elif, ..., else
    """
    def __init__(self):
        self.result = None
    def match(self,pattern,text):
        self.result = match(pattern,text)
        return self.result
    def search(self,pattern,text):
        self.result = search(pattern,text)
        return self.result


class ScrollSubWin(object):
             
    def __init__(self, parent_win, hi_left_y, hi_left_x, text):
        self.parent = parent_win
        max_y, max_x = self.parent.getmaxyx()
        self.parent_bot = max_y-1
        self.hly = hi_left_y
        self.hlx = hi_left_x
        self.nlines = max_y - self.hly - 1
        self.ncols = max_x - self.hlx - 2
        self.screen = parent_win.subwin(self.nlines, self.ncols, self.hly, self.hlx)
        self.screen.keypad(1) 
        self.screen.border(0)
        self.text = text
        self.topLineNum = 0
        self.highlightLineNum = 0
        self.DOWN = 1
        self.UP = -1
        self.SPACE_KEY = 32
        self.ESC_KEY = 27
        self.outputLines = []
        self.getOutputLines()
        
    def run(self):
        while True:
            self.displayScreen()
            # get user command
            c = self.screen.getch()
            if c == curses.KEY_UP: 
                self.updown(self.UP)
            elif c == curses.KEY_DOWN:
                self.updown(self.DOWN)
            else:
                return

    def getOutputLines(self):
        if self.text is not str:
            self.text = str(self.text)
        text = self.text.split('\n')        
        for i, line in enumerate(text):
            wrap_list = textwrap.wrap(line, 
                                      width=Globals.MAX_LINE_CHARS-5,
                                      subsequent_indent = ' ' * 5)
            self.outputLines.extend(wrap_list)
        self.nOutputLines = len(self.outputLines)

    def displayScreen(self):
        # clear screen
        self.screen.erase()
        # now paint the rows
        top = self.topLineNum
        bottom = self.topLineNum + self.nlines
        logger.debug("Top: {}, bottom: {}".format(top, bottom))
        for (index,line) in enumerate(self.outputLines[top:bottom]):
            linenum = self.topLineNum + index
            # highlight current line  
            if index != self.highlightLineNum:
                self.screen.addstr(index, 0, line)
            else:
                self.screen.addstr(index, 0, line, curses.A_BOLD)
        self.screen.refresh()
        top_mess = "Scroll Mode ON" 
        bot_mess = "MOVE: Up/Down arrow. EXIT: Any key. TIP: run 'ls' at prompt."
        top_y = self.parent_bot - self.nlines - 2
        self.parent.addstr(top_y, 5, top_mess, Globals.BLUE_BACK)
        self.parent.addstr(self.parent_bot, 3, bot_mess, Globals.BLUE_BACK)
        self.parent.refresh()


    # move highlight up/down one line
    def updown(self, increment):
        nextLineNum = self.highlightLineNum + increment
        
        # paging
        if increment == self.UP and self.highlightLineNum == 0 and self.topLineNum != 0:
            self.topLineNum += self.UP 
            return
        elif increment == self.DOWN and nextLineNum == self.nlines and (self.topLineNum+self.nlines) != self.nOutputLines:
            self.topLineNum += self.DOWN
            return

        # scroll highlight line
        if increment == self.UP and (self.topLineNum != 0 or self.highlightLineNum != 0):
            self.highlightLineNum = nextLineNum
        elif increment == self.DOWN and (self.topLineNum+self.highlightLineNum+1) != self.nOutputLines and self.highlightLineNum != curses.LINES:
            self.highlightLineNum = nextLineNum
 
    def __del__(self):
        self.outputLines = []


class Menu(object):                                                          

    def __init__(self, items, stdscreen, exit_label='Exit', title=''):
        self.window = stdscreen
        self.window.keypad(1)                    
        curses.curs_set(0)
        self.panel = panel.new_panel(self.window)
        self.window.border(0)
#        self.panel.hide()                                                    
        panel.update_panels()                                                
        self.position = 0                                                    
        self.items = items                         
        self.items.append((exit_label,'exit'))
        self.origin_y = 5
        self.origin_x = 10
        self.title = title

    def navigate(self, n):                                                   
        self.position += n                                                   
        if self.position < 0:                                                
            self.position = 0                                                
        elif self.position >= len(self.items):                               
            self.position = len(self.items)-1                                

    def display(self):                                                       
        self.panel.top()                                                     
        self.panel.show()                                                    
        self.window.clear()                                                  

        while True:                                                          
            self.window.refresh()                                            
            curses.doupdate()                                                
            
            self.window.addstr(self.origin_y-2, self.origin_x-3, self.title, curses.A_BOLD)                    
            
            for index, item in enumerate(self.items):                        
                if index == self.position:                                   
                    mode = curses.A_REVERSE                                  
                else:                                                        
                    mode = curses.A_NORMAL                                   

                msg = '%d. %s' % (index+1, item[0])                            
                self.window.addstr(self.origin_y+index, self.origin_x, msg, mode)                    

            key = self.window.getch()                                        

            if key in [curses.KEY_ENTER, ord('\n')]:                         
                if self.position == len(self.items)-1:                       
                    break                                                    
                else:                                                        
                    self.items[self.position][1]()                           

            elif key == curses.KEY_UP:                                       
                self.navigate(-1)                                            

            elif key == curses.KEY_DOWN:                                     
                self.navigate(1)                                             

        self.window.clear()                                                  
        self.panel.hide()                                                    
        panel.update_panels()                                                
        curses.doupdate()




def get_section_from_initial(letter):
    valid_initials = Globals.VALID_SECTION_INITIALS
    if letter in valid_initials:
        return Globals.VALID_SECTIONS[valid_initials.index(letter)]

def get_color_from_initial(letter):
    if letter.lower() == 'v':
        return Globals.GREEN_FG_BOLD 
    elif letter.lower() == 'c':
        return Globals.RED_FG_BOLD
    elif letter.lower() == 'b':
        return Globals.BLUE_FG_BOLD
    elif letter.lower() == 'm':
        return Globals.MAGENTA_FG_BOLD
    else:
        return curses.A_NORMAL


def get_section_initial_from_type(type_word):
    """
    :rtype: str
    :return: Section letter or error message if invalid section type specified
    """
    valid_types = Globals.VALID_SECTIONS
    type_word = type_word.lower()
    if type_word in valid_types:
        return type_word[0].upper()
    else:
        return 'ERROR:Invalid section specified.'
    
def run_unix_cmd(cmd):
    """
    Run Unix(-like) shell command.

    :type cmd: str
    :param cmd: command to run

    :rtype: tuple
    :return: (stdoutdata, stderrdata)
    """
    import shlex, subprocess
    args = shlex.split(cmd)
    p = subprocess.Popen(args, 
                         stdout=subprocess.PIPE, 
                         stderr=subprocess.PIPE)
    logger.debug("Running command: {}".format(cmd))
    return  p.communicate()

def tty_size_ok():
    """
    Only works on Unix-like OSes.
    
    :rtype: boolean
    :return: True if OK
             False otherwise
    """
    logger.info("Getting tty size...")
    #get tty size via OS command
    tty_size_cmd = 'stty size'
    tty_size, err = run_unix_cmd(tty_size_cmd)
    if err:
        logger.error(err)
    tty_size_l = tty_size.split()
    logger.debug("Got tty_size: {}".format(tty_size))
    
    # try to resize tty
    logger.info("Tty size not acceptable. Resizing...")
    if Globals.MIN_TTY_LINES > int(tty_size_l[0]) or Globals.MIN_TTY_COLS > int(tty_size_l[1]):
        resize_cmd = "resize -s {} {}".format(Globals.MIN_TTY_LINES, Globals.MIN_TTY_COLS)
        try:
            output, err = run_unix_cmd(resize_cmd)
            if err:
                logger.error(err.strip())
                return False
        except OSError as e: 
            logger.info("Command 'resize' might not be available...")
            return False
    logger.info("Done.")
    return True
    
     
def init_curse():
    """
    Initialize curses printing
    """
    if not tty_size_ok():
        logger.error("Failed to set TTY to optimum size.")
        logger.info("Please make sure your TTY is at least {} lines by {} cols in size and try running the program again.".format(Globals.MIN_TTY_LINES, Globals.MIN_TTY_COLS))
        exit(1)
    window = curses.initscr()
    # don't display typed chars
    curses.noecho()              
    # don't wait for enter to read chars
    curses.cbreak()
    # detect special keys, such as the cursor keys or navigation keys
    window.keypad(1)
    # start color
    curses.start_color()
    curses.use_default_colors()
    # define some colors
    logger.debug("CAN CHANGE COLOR:{}".format(curses.can_change_color()))
    # custom pairs
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_RED)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_GREEN)
    curses.init_pair(3, curses.COLOR_RED, -1) #red fg, default bg
    curses.init_pair(4, curses.COLOR_GREEN, -1) #green fg, default bg
    curses.init_pair(5, curses.COLOR_BLUE, -1) #blue fg, default bg
    curses.init_pair(6, curses.COLOR_MAGENTA, -1) #magenta fg, default bg
    curses.init_pair(7, curses.COLOR_BLACK, curses.COLOR_MAGENTA)
    curses.init_pair(8, curses.COLOR_BLACK, curses.COLOR_BLUE)
    curses.init_pair(9, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(10, curses.COLOR_BLACK, curses.COLOR_YELLOW)
    curses.init_pair(11, curses.COLOR_CYAN, -1) #cyan fg, default bg
    try:
        # 256 color section
        curses.init_pair(12, 246, -1) #grey fg, default bg
    except:
        # terminal does not support 256 colors
        curses.init_pair(12, -1, -1) #use defaults
    curses.init_pair(13, curses.COLOR_BLACK, -1) #black fg, default bg

    # background simple
    Globals.RED_BACK = curses.color_pair(1)
    Globals.GREEN_BACK = curses.color_pair(2)
    Globals.MAGENTA_BACK = curses.color_pair(7)
    Globals.BLUE_BACK = curses.color_pair(8)
    Globals.CYAN_BACK = curses.color_pair(9)
    Globals.YELLOW_BACK = curses.color_pair(10)
    # background bold
    Globals.BOLD_RED_BACK = curses.color_pair(1) | curses.A_BOLD
    Globals.BOLD_GREEN_BACK = curses.color_pair(2) | curses.A_BOLD
    Globals.BLUE_BACK_BOLD = curses.color_pair(8) | curses.A_BOLD
    # foreground bold
    Globals.RED_FG_BOLD = curses.color_pair(3) | curses.A_BOLD
    Globals.GREEN_FG_BOLD = curses.color_pair(4) | curses.A_BOLD
    Globals.BLUE_FG_BOLD = curses.color_pair(5) | curses.A_BOLD
    Globals.MAGENTA_FG_BOLD = curses.color_pair(6) | curses.A_BOLD
    Globals.CYAN_FG_BOLD = curses.color_pair(11) | curses.A_BOLD
    Globals.GREY_FG_BOLD = curses.color_pair(12) | curses.A_BOLD
    Globals.BLACK_FG_BOLD = curses.color_pair(13) | curses.A_BOLD 
    # foreground simple
    Globals.RED_FG = curses.color_pair(3)
    Globals.MAGENTA_FG = curses.color_pair(6)
    Globals.GREEN_FG = curses.color_pair(4)
    Globals.BLUE_FG = curses.color_pair(5)
    Globals.CYAN_FG = curses.color_pair(11)
    Globals.GREY_FG = curses.color_pair(12)

    Globals.RLINK_COLORS = ["NOCOLOR",
                            Globals.RED_BACK,
                            Globals.GREEN_BACK,
                            Globals.MAGENTA_BACK,
                            Globals.BLUE_BACK,
                            Globals.CYAN_BACK,
                            Globals.YELLOW_BACK]
    return window

def end_curse(window):
    """
    Uninitialize curses printing (so that the shell will be usable 
    after program end)
    """
    window.keypad(0)
    window.erase()
    curses.nocbreak()
    curses.echo()
    curses.endwin()

def clear_line(window, y, x=0):
    """
    Clear line y starting from position x
    """
    window.move(y, x)
    window.clrtoeol()
    window.border(0)

def add_multiline_text(window, hl_y, hl_x, text, format=curses.A_NORMAL):
    """
    Add multiline text to a window.
    Intended to be used with bordered windows in order
    to avoid border being overwritten 

    !!! Does not perform refresh on the window 1!!

    :type window: curses window
    :param window: window to add text to

    :type hl_y, hl_x: int, int
    :param hl_y, hl_x: coordinates for high left corner where 
                      the first line of text will begine

    :type text: str
    :param text: text to be added to window
    """
    if text is not str:
        text = str(text)
    text = text.split('\n')
    offset = 0
    for i, line in enumerate(text):
        text_list = textwrap.wrap(line, 
                                  width=Globals.MAX_LINE_CHARS-5,
                                  subsequent_indent = ' ' * 5)
        for index, text in enumerate(text_list):
            y = hl_y + i + index + offset
            window.addstr(y, hl_x, text, format)
            capital_line = text.upper()
            if capital_line.startswith('ERROR:'):
                window.addstr(y, hl_x, 'ERROR:', Globals.RED_FG)
            elif capital_line.startswith('WARNING:'):
                window.addstr(y, hl_x, 'WARNING:', Globals.MAGENTA_FG)
            elif capital_line.startswith('SUCCESS:'):
                window.addstr(y, hl_x, 'SUCCESS:', Globals.GREEN_FG)
            # bold red MISSING
            idx = text.find('MISSING')
            if idx != -1:
                window.addstr(y, hl_x+idx, 'MISSING', Globals.RED_FG_BOLD) 
        if index > 0:
            offset = index

def get_text(window, prompt, comment, y, x, length_limit=0):
    """
    Prompt user for input and capture and return input text
    """
    curses.curs_set(1)
    window.addstr(y, x, prompt)
    if comment:
        add_multiline_text(window, y+2, x, comment, curses.A_DIM)
    curses.echo()
    if length_limit == 0:
        input = window.getstr(y, x+len(prompt))
    else:
        input = window.getstr(y, x+len(prompt), length_limit)
    clear_line(window, y)
    curses.noecho()
    curses.curs_set(0)
    return input


def do_cli_prompt(window, prompt, y, x):
    window.addstr(y, x, prompt)
    window.move(y, x+len(prompt))
    window.clrtoeol()
    window.border(0)
    curses.curs_set(1) 
    window.refresh()            
    curses.echo()
    # handle not printable chars
    char = window.getch()
    instr = ''
    min_y, min_x = curses.getsyx()
    aux_x = min_x - 1
    while True:
        # limit input char number and handle esc
        if len(instr) == Globals.MAX_CLI_INPUT_CHARS:
            accept_input = False
        elif char == curses.ascii.ESC:
            if instr:
                instr = ''
            else:
                curses.beep()
            break
        else:
            accept_input = True
            curses.echo()
        # handle input
        if char in (curses.ascii.BS, curses.KEY_BACKSPACE):
            if len(instr) > 0:
                instr = instr[:-1]
                clear_line(window, y)
                window.addstr(y, x, prompt+instr)
            else:
                curses.beep()
                break
        elif curses.ascii.isprint(char):
            if accept_input:
                aux_x += 1
                instr += chr(char)
            else:
                curses.beep()
        elif char == curses.KEY_RIGHT and not instr: 
            instr = 'KEY_RIGHT'
            break
        elif char == curses.KEY_LEFT and not instr:
            instr = 'KEY_LEFT'
            break
        elif char == curses.KEY_UP: 
            if Globals.HISTORY_INDEX >= 0:
                if Globals.HISTORY_INDEX >= len(Globals.CMD_HISTORY):
                    instr = Globals.PREV_CMD
                else:           
                    instr = Globals.CMD_HISTORY[Globals.HISTORY_INDEX]
                if not instr: 
                    instr = ""
                    curses.beep()
                window.addstr(y, x, prompt+instr)
                window.clrtoeol()
                Globals.HISTORY_INDEX -=1
            else:
                curses.beep()
        elif char == curses.KEY_DOWN:
            max_len = len(Globals.CMD_HISTORY)
            if Globals.HISTORY_INDEX == -1:
                Globals.HISTORY_INDEX = 0
            if Globals.HISTORY_INDEX <= max_len-1:
                Globals.HISTORY_INDEX +=1
                if Globals.HISTORY_INDEX >= max_len:
                    instr = Globals.PREV_CMD
                else:
                    instr = Globals.CMD_HISTORY[Globals.HISTORY_INDEX]
                window.addstr(y, x, prompt+instr)
                window.clrtoeol()
            else:
                Globals.HISTORY_INDEX == max_len
                curses.beep()
        elif char in [curses.ascii.NL, curses.ascii.CR, curses.ascii.LF]:
            Globals.HISTORY_INDEX=len(Globals.CMD_HISTORY)+1
            break
        char = window.getch()
    curses.noecho()
#    curses.curs_set(0) 
#    clear_line(window, y)
    return instr

def check_positive_int(input, max_limit='', min_limit=0):
    if is_number(input):
        if is_number(max_limit):
            if is_number(min_limit):
                if int(max_limit) > int(input) > int(min_limit):
                    return True
            if int(max_limit) > int(input):
                return True
        else:
            if int(input) > 0:
                return True
    return False

def shelf_clear():
    """
    Delete the do/undo database at program start.
    """
    logger.debug("Deleting old shelf database '{}'".format(Globals.SHELF))
    try:
        os.remove(Globals.SHELF)
    except OSError:
        logger.debug("Nothing to delete. File does not exist.")

def shelf_put(key, song):
    """
    Save the current song state in the do/undo database

    :type key: int or str
    :param key: shelf database key (must be unique)

    :type song: Song
    :param song: the song instance before running 'command'
    """
    s = shelve.open(Globals.SHELF)
    try:
        s[str(key)] = song
    except Exception as e:
        log_exception(e)
    s.close()

def shelf_get(key):
    """
    Get element from do/undo shelf database.

    :type key: int or str
    :param key: shelf database key (must exist in database, otherwise an
                exception will be recieved in the logfile)
                
    :rtype: (Song, str) or None
    :return: (song, cmd) - song object and command run when song was 
                           at the saved state
             None -  some exception was                
    """
    s = shelve.open(Globals.SHELF)
    try:
        ret = s[str(key)]
    except Exception as e:
        log_exception(e)
        ret = None
    s.close()
    return ret

def log_exception(e, message=None):
    if message: 
        message += "\n\tGot Exception >>"
    else: 
        message = "Got Exception >>"
    logger.exception("{} \n {}".format(message, e))

def get_random_line(file_name, words_per_line=0):
    """
    Get a random line from a text file.

    :type file_name: str
    :param file_name: A string containing a valid txt file name

    :type words_per_line: int
    :param words_per_line: -optional argument - number of words returned line
                           should contain. Will be ignorred if set to 0 or is 
                           the input file doesnot contain any lines with that 
                           number of words.
    Notes:
        If words_per_line not 0, all lines from file_name that contain the 
            specified number of words will be written to a temp file and that
            temp file will be used as the input file (to ensure radomization
            among the lines containing the desired number of words)
        If file_name has no lines containing the desired number of words, then
            a random line will be selected and worde_per_line will be ignorred.
    """
    # get temp file containing lines with desired number of words
    if words_per_line:
        wrote_to_tmp = 0
        with open(Globals.TEMP_TXT, 'w') as tmp:
            with open (file_name, 'r') as afile:
                for line in afile:
                    if get_word_count(line) == words_per_line:
                        tmp.write(line)
                        wrote_to_tmp = 1
        # if no aproproate line found, don't use the tmp file
        if wrote_to_tmp: 
           file_name = Globals.TEMP_TXT
    # get random line
    with open(file_name, 'r') as afile:
        line = next(afile)
        for num, aline in enumerate(afile):
            if random.randrange(num + 2): continue
            line = aline
    return line.strip()


def get_last_line_randomize_cmd():
    for cmd in reversed(Globals.CMD_HISTORY):
        if cmd.split()[0] == 'rl':
            return cmd

def strip_word(word):
    lower_word = word.lower()
    stripped_word = lower_word.strip()
    ret_word = stripped_word.strip(string.punctuation)
    return ret_word

def is_number(s):
    try:
        float(s)
    except ValueError:
        return False
    else:
        if str(s).lower() in ['nan', 'inf', '-inf']:
            return False
        return True

def get_syll_count(word):       
    """
    Count syllables in a word.
    
    Uses the The Carnegie Mellon Pronouncing Dictionary [cmudict.0.6] 
    (Copyright 1998 Carnegie Mellon University) to look at the pronunciation of
    the word and counts the stressed vowels to come up with the syllable count.

    If the word is not in the CMUDICT, then use a few super simple rules:
        - a vowel starts each syllable;
        - a doubled vowel doesn't add an extra syllable;
        - two or more different vowels together are a diphthong,
        - and probably don't start a new syllable but might;
        - y is considered a vowel when it follows a consonant.
    (Copyright 2013 by Akkana Peck http://shallowsky.com.
    Share and enjoy under the terms of the GPLv2 or later.)    

    :rtype: [int, int, ...]
    :return: list of ints containig syllable count for each different 
                  pronunciation of the word in word in cmudict,
             or list of min and max syllable count if word not in cmudict 
    """
    word = strip_word(word)
    if not word: 
        return [0, 0] 
    try:
        # count vowel phonemes in pronuciation (they end in 0, 1, or 2)
        return [len([phoneme for phoneme in pronunciation 
                     if is_number(phoneme[-1])]) 
                for pronunciation in Globals.CMUDICT[word]]
    except KeyError:
        # if word not in dict
        vowels = ['a', 'e', 'i', 'o', 'u']
        on_vowel, in_diphthong = False, False
        minsyl, maxsyl = 0, 0
        lastchar = None
        for c in word:
            is_vowel = c in vowels
            if on_vowel == None:
                on_vowel = is_vowel
            # y is a special case
            if c == 'y':
                is_vowel = not on_vowel
            if is_vowel:
                #if verbose: print c, "is a vowel"
                if not on_vowel:
                    # We weren't on a vowel before.
                    # Seeing a new vowel bumps the syllable count.
                    #if verbose: print "new syllable"
                    minsyl += 1
                    maxsyl += 1
                elif on_vowel and not in_diphthong and c != lastchar:
                    # We were already in a vowel.
                    # Don't increment anything except the max count,
                    # and only do that once per diphthong.
                    #if verbose: print c, "is a diphthong"
                    in_diphthong = True
                    maxsyl += 1
            #elif verbose: print "[consonant]"
            on_vowel = is_vowel
            lastchar = c
    # Some special cases:
    if word[-1] == 'e':
        minsyl -= 1
    # if it ended with a consonant followed by y, count that as a syllable.
    if word[-1] == 'y' and not on_vowel:
        maxsyl += 1
    return [minsyl, maxsyl]


def get_word_list(text):
    """
    Return simple word list, punctuation excluded.

    Note: Use 'tokenize' if punctuation needs to be kept
    """
    return re.findall(r'[0-9a-zA-Z_\']+', text)


def get_word_count(text):
    return len(get_word_list(text))


def get_text_syll_count(text):
    """
    Count sillables in a text (one or more lines and puctuation).
    """
    return sum([max(get_syll_count(word)) for word in get_word_list(text)])

@timer
def get_random_word(required_syll_count=0, use_dict='top5000', rmode=0):
    """
    Get a lowercase random word from the dictionary specified by 'use_dict'.

    :type required_syll_count: int
    :param required_syll_count: if <= 0 - random word from dict will be retured
                                if > 0 - the random word will have the desired
                                        number of syllables
    :type use_dict: str
    :param use_dict: dict to take word from. (only used with rmodes 1 and 3,
                             or when rmode 2 fails to yield the needed word)
                     Values:  - top5000 - dictionary containing the 5000 most 
                                          frequent english words
                              - cmudict - nltk dict containing 130,000+ entries

    :type rmode: int
    :param rmode: run with specific randomize mode instead of using Globals.RMODE.
                        Will use Globals.RMODE and log a warning if invalid 
                        randomize mode is specified.
                        
    NOTE: If the specified dict contains no words with the required number of 
          syllables, a random word will be returned from the top5000 dict.
    """ 

    if rmode not in Globals.VALID_RMODES:
        logger.debug("WARNING: Invalid rmode ({}) provided. Using Globals.RMODE ({}).".format(
                rmode, Globals.RMODE))
        rmode = Globals.RMODE
    logger.debug("Using local rmode: {} (global rmode: {}).".format(rmode, Globals.RMODE))
    logger.debug("Looking for a {} syllable word.".format(required_syll_count))

    if rmode == 1:
        # use dict
        logger.debug("Getting word from {}...".format(use_dict))
        if use_dict == 'top5000':
            words = Globals.TOP5000_WORDS
            words_by_syllcount = Globals.TOP5000_WBS
        else:
            words = Globals.CMUDICT_WORDS
            words_by_syllcount = Globals.CMUDICT_WBS                 
    elif rmode == 2:
        # use ra files
        logger.debug("Getting word from RA files...")        
        words = Globals.RA_WORDS
        words_by_syllcount = Globals.RA_WBS
    else:
        # use round robin dict/syllable files 
        if Globals.LAST_RMODE_USED == 1:
            Globals.LAST_RMODE_USED = 2
        else:
            Globals.LAST_RMODE_USED = 1
        return get_random_word(required_syll_count, 
                               rmode=Globals.LAST_RMODE_USED)
        
    if required_syll_count <= 0:
        if not words:
            logger.debug("Word list empty, using TOP5000 to get random word...")
            words = Globals.TOP5000_WORDS
        ret_word = random.choice(words)
    else:
        word_candidates = words_by_syllcount[required_syll_count]
        if not word_candidates:
            logger.debug("WARNING: No suitable word found. Generating random sillable word.")
            ret_word = get_random_word()
        else:
            ret_word = random.choice(word_candidates)
    return ret_word.lower()


def get_pron(word):
    """
    Return word pronunciation(s)
    """
    word = strip_word(word)
    try:
        return Globals.CMUDICT[word]
    except:
        if '-' in word:
            word = word.replace('-', '')
            return get_pron(word)
        else:
            logger.debug("ERROR:No pronunciation found for: '{}'".format(word))
            return None
        
        
def get_rhyme(rhyme_with, syll_count=0):
    """
    Get a random word that has 'syll_count' syllables and rhymes with the
    'rhyme_with' word.
    """
    # get pronunciation of rhyme_with
    pronunciations = get_pron(rhyme_with)
    logger.debug("pronunciations: {}".format(pronunciations))
    rhymes = []
    if not pronunciations:
        logger.debug("No pronunciation in cmudict for '{}'".format(rhyme_with))
        return None
    for pron in pronunciations:
        last_syll = []
        # get last sillable in pronunciation
        for index, phoneme in enumerate(reversed(pron)):
            #logger.debug("pron: {}\nphoneme: {}\n".format(pron, phoneme))
            if phoneme[-1] in string.digits:
                if index == 0:
                    index = 1
                last_vowel_idx = index + 1
                last_syll = pron[-last_vowel_idx:]
                break
        # get a list of words that have an identical last syllable
        for word in Globals.TOP5000_WORDS:
            if word:
                prons = get_pron(word)
                if prons:
                    for pron in prons:
                        if (pron[-last_vowel_idx:] == last_syll
                            and word not in rhymes 
                            and word != rhyme_with):
                            rhymes.append(word)
    logger.debug(rhymes)
    if not rhymes:
        return get_random_word(syll_count)
    return random.choice(rhymes)


def join_punctuation(seq, characters=string.punctuation):
    characters = set(characters)
    seq = iter(seq)
    current = next(seq)
    for nxt in seq:
        if nxt in characters:
            current += nxt
        else:
            yield current
            current = nxt
    yield current


def tokenize(text):
    """
    Tokenize a text, but keep punctuation joined with the words.
    
    Note: Used when punctuation needs to be kept.
    """
    pattern = r'''(?x) # set flag to allow verbose regexps
([A-Z]\.)+                 # abbreviations, e.g. U.S.A.
| \w+([-']\w+)*            # words with optional internal hyphens
| \$?\d+(\.\d+)?%?         # currency and percentages, e.g. $12.40, 82%
| \.\.\.                   # ellipsis
| [][.,;"'?():-_`]         # these are separate tokens
'''
    #logger.debug("tokenizing: {}".format(text))
    tokens = nltk.regexp_tokenize(text, pattern)
    for index, token in enumerate(tokens):
        if token == "i" or token.startswith("i'"):
            tokens[index] = token.replace('i', 'I')
    #logger.debug("tokens:{}".format(tokens))
    ret = list(join_punctuation(tokens))
    #logger.debug("joined:{}".format(ret))
    return ret


def get_syll_lists(word_list):
    """
    Return a list of tuples each containing words grouped by syllable number
    Ex: return list will contain on position 5 a list consisting of all
        the words in word_list with the syllable count of 5
    """
    word_sets = [set() for x in range(0,50)]
    for word in word_list:
        indexes = get_syll_count(word)
        for index in indexes:
            word_sets[index].add(word)
    return [tuple(word_set) for word_set in word_sets]


def flatten(list_of_lists):
    return [item for a_list in list_of_list for item in a_list]


@timer
def get_cmu_wbs():
    Globals.CMUDICT_WBS = get_syll_lists(Globals.CMUDICT_WORDS)

@timer
def get_top_wbs():
    with open(Globals.TOP5000_DICT) as d:
        Globals.TOP5000_WORDS = tuple([word.strip() 
                                       for word in d.readlines()])
    Globals.TOP5000_WBS = get_syll_lists(Globals.TOP5000_WORDS)

@timer
def get_ra_wbs():
    ra_words = set()
    for syll_file in os.listdir(Globals.RA_PATH):
        with open(os.path.join(Globals.RA_PATH, syll_file)) as sfile:
            ra_words.update([word.strip() for line in sfile.readlines() 
                             for word in get_word_list(line)])
    Globals.RA_WORDS = tuple(ra_words)
    Globals.RA_WBS = get_syll_lists(Globals.RA_WORDS)

def show_editbox(window, nlines, ncols, y, x, initial_text=''):
    """
    Shows a text box  that allows text input/edit.

    :type window: curses window
    :param window: curses main window that will contain the text box window 

    :type nlines, ncols: int
    :param nlines, ncols: number of lines and columns of the text box window

    :type y, x: int
    :param y, x: coordinates for the upper left corner of the text box window

    :type initial_text: str (optional argument)
    :param initial_text: text initially  present in the textbox
        
    :rtype: str
    :return: The string present in the box when Enter or Ctrl+G was pressed

    :DETAILS: (from curses.textpad module)

     Supports the following Emacs-like key bindings:

    Ctrl-A      Go to left edge of window.
    Ctrl-B      Cursor left, wrapping to previous line if appropriate.
    Ctrl-D      Delete character under cursor.
    Ctrl-E      Go to right edge (stripspaces off) or end of line (stripspaces on).
    Ctrl-F      Cursor right, wrapping to next line when appropriate.
    Ctrl-G      Terminate, returning the window contents.
    Ctrl-H      Delete character backward.
    Ctrl-J      Terminate if the window is 1 line, otherwise insert newline.
    Ctrl-K      If line is blank, delete it, otherwise clear to end of line.
    Ctrl-L      Refresh screen.
    Ctrl-N      Cursor down; move down one line.
    Ctrl-O      Insert a blank line at cursor location.
    Ctrl-P      Cursor up; move up one line.

     Additional bindings:

    Enter       Same as Ctrl-G
    Esc         Discard all changes to initial text

    Move operations do nothing if the cursor is at an edge where the movement
    is not possible.  The following synonyms are supported where possible:

    KEY_LEFT = Ctrl-B, KEY_RIGHT = Ctrl-F, KEY_UP = Ctrl-P, KEY_DOWN = Ctrl-N
    KEY_BACKSPACE = Ctrl-h
    """

    if len(initial_text) >= Globals.MAX_LINE_CHARS:
        initial_text = initial_text[:Globals.MAX_LINE_CHARS-1]

    edit_win = curses.newwin(nlines, ncols, y, x)
    textpad.rectangle(window, y-1, x-1, y+nlines, x+ncols)
    curses.curs_set(1)
    curses.noecho()
    edit_win.keypad(1)
    textbox = textpad.Textbox(edit_win, insert_mode=True)
    edit_win.addstr(0,0,initial_text)
    window.refresh()
    def exit_edit (ch):
        if ch in (curses.ascii.CR, curses.ascii.LF):
            Globals.EDIT_EXIT_KEY = 'None'
            return curses.ascii.BEL
        if ch in (curses.ascii.DEL, curses.KEY_DC):
            return curses.ascii.EOT
        elif ch == curses.ascii.ESC:
            Globals.EDIT_EXIT_KEY = 'ESC'
            return curses.ascii.BEL
        elif ch == curses.KEY_UP:
            Globals.EDIT_EXIT_KEY = 'UP'
            return curses.ascii.BEL
        elif ch == curses.KEY_DOWN:
            Globals.EDIT_EXIT_KEY = 'DOWN'
            return curses.ascii.BEL
        else:
            return ch
    # do edit
    return textbox.edit(exit_edit)


def show_text(window, text):
    """
    Show text in a scrollable pad on the specified window.

    Keys:
       q = exit
       Arrow Up, Arrow Down, PgUp, PgDn, Home & End = scroll text 
    """
    window.clear()
    window.border(0)
    wy, wx = window.getmaxyx()
    wy-=2
    wx-=2
    padx = max(getmax(text),wx)
    pady = max(len(text)+1,wy)
    max_x = padx-wx
    max_y = pady-wy
    pad = curses.newpad(pady,padx)

    x, y, inkey = 0, 0, 0
    add_multiline_text(pad, x, y, text)
    # show text before pressing any key
    window.refresh()
    # handle key press
    while inkey != 'q':
        pad.refresh(y,x,2,5,wy,wx)
        inkey = window.getkey()
        if inkey=='KEY_UP':y=max(y-1,0)
        elif inkey=='KEY_DOWN':y=min(y+1,max_y)
        elif inkey=='KEY_LEFT':x=max(x-1,0)
        elif inkey=='KEY_RIGHT':x=min(x+1,max_x)
        elif inkey=='KEY_NPAGE':y=min(y+wy,max_y)
        elif inkey=='KEY_PPAGE':y=max(y-wy,0)
        elif inkey=='KEY_HOME':y=0
        elif inkey=='KEY_END':y=max_y        

def getmax(lines): 
    return max([len(str(l)) for l in lines])

def count_lines(file_name):
    """
    Return number of non blank lines in file
    """
    with open(file_name) as f:
        return sum(1 for line in f if line.strip())


def do_warning(window, question, comment=""):
    window.clear()
    window.border(0)    
    window.addstr(0, 5, "WARNING!", curses.A_UNDERLINE)
    y = Globals.HEAD_Y + 7
    x = Globals.HEAD_X + 10
    return get_text(window, question, comment, y, x, length_limit=1)
