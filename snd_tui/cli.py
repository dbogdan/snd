from time import sleep
from collections import OrderedDict
from copy import deepcopy

from snd_tui.lib import *
from snd_conf import logger
from snd_conf import Globals
from snd_tui.help import man

MESSAGE = '' # message to display after each command run


def print_screen(window):
    """
    Print the song section that is marked as active, snd prompt, 
    and any user messages
    
    :type window: curses window
    """
    global last_line_y
    global MESSAGE 

    window.clear()

    # show title
    window.addstr(Globals.TITLE_Y, Globals.TITLE_X, Globals.SONG.name, curses.A_BOLD)

    # show section letters
    structure = ''.join([sect.type[0].upper() for sect in Globals.SONG.sections])
    window.addstr(Globals.TITLE_Y, Globals.STRUCTURE_X, structure, curses.A_BOLD)

    # show active section
    if len(Globals.SONG.sections):
        active_section_index = Globals.SONG.get_active_section_index()
        active_section_x  = Globals.STRUCTURE_X + active_section_index
        active_section_type = Globals.SONG.get_active_section_type()
        type_letter = get_section_initial_from_type(active_section_type)
        #show active section in color
        display_color = get_color_from_initial(type_letter)
        window.addstr(Globals.TITLE_Y, active_section_x, type_letter, display_color)
        window.addstr(Globals.TITLE_Y+1, active_section_x, "^", curses.A_BOLD) 
        # show summary header
        window.addstr(Globals.HEAD_Y, Globals.HEAD_X, "Summary", curses.A_BOLD)
        HEAD2_Y = Globals.HEAD_Y + 1
        window.addstr(HEAD2_Y, Globals.HEAD_X, "Wds/Syl", curses.A_BOLD)
        # show section name
        active_section_type = Globals.SONG.get_active_section_type()
        active_section_order = 1
        for section in Globals.SONG.sections[:active_section_index]:
            if section.type == active_section_type:
                active_section_order += 1
        text = '{} {}'.format(active_section_type, active_section_order)
        window.addstr(HEAD2_Y, Globals.SECTION_NAME_X, text.capitalize(), display_color)

        # show lines
        active_section = Globals.SONG.get_active_section()
        last_line_y = Globals.FIRST_LINE_Y
        for line in active_section.lines:
            # show line info
            if not line.is_enabled:
                summ_attr = curses.A_BOLD
            else:
                summ_attr = curses.A_NORMAL
            # line numbers
            if Globals.SHOW_LINE_NUMBERS:
                line_no_X = Globals.HEAD_X - 5
                line_no = "{:2}".format(line.index+1)
                window.addstr(last_line_y, line_no_X, "(")
                window.addstr(last_line_y, line_no_X+1, line_no, Globals.BLUE_FG)
                window.addstr(last_line_y, line_no_X+3, ")")    
            # word count
            word_count = "{:2}/".format(line.word_count)
            content_X = Globals.HEAD_X
            window.addstr(last_line_y, content_X, word_count, summ_attr)
            # actual syllable count
            content_X += len(word_count)
            actual_syll_count = "{:2}".format(line.actual_syll_count)
            asc_color = curses.A_NORMAL
            syll_dif = line.actual_syll_count - line.required_syll_count 
            if abs(syll_dif) >= 3:
                syll_attr = Globals.RED_FG
            else:
                syll_attr = summ_attr
            window.addstr(last_line_y, content_X, actual_syll_count, syll_attr)
            # required syll count and enabled mark
            content_X += len(actual_syll_count)
            enabled_mark = " "
            if line.is_enabled:
                enabled_mark = "X"
            summary_rest = ".{:2} [{}] \t".format(line.required_syll_count, 
                                                 enabled_mark)
            window.addstr(last_line_y, content_X, summary_rest, summ_attr)
            # show summary in black bold if line is off
            if not line.is_enabled:
                summary = "{:2}/{:2}.{:2} [ ] \t".format(line.word_count,
                                                       actual_syll_count,
                                                       line.required_syll_count)
                window.addstr(last_line_y, Globals.HEAD_X, summary, Globals.BLACK_FG_BOLD)
            # show line content
            content_X += len(summary_rest) 
            locked_attr=Globals.BLACK_FG_BOLD
            if line.has_locked_words and not line.is_locked:
                try:
                    first_word = line.words[0].get_value()
                    line.words[0].set_value(first_word.capitalize())
                except IndexError:
                    pass
                aux_X = content_X
                for word in line.words:
                    attr = Globals.GREY_FG
                    if word.is_locked:
                        attr = locked_attr
                    add_word = word.value + " "
                    window.addstr(last_line_y, aux_X, add_word, attr)
                    aux_X += len(add_word)
            else:
                attr = Globals.GREY_FG
                if line.is_locked:
                    attr = locked_attr
#                elif not line.is_enabled:
#                    attr = Globals.GREY_FG_BOLD
                window.addstr(last_line_y, content_X, line.content, attr)
            # show rlinks
            if line.rlink and Globals.SONG.show_rlinks and line.is_enabled:
                word = line.words[line.rlink_word_idx]
                word_value = word.get_value()
                pre_rlink = line.get_content().split(word_value)[0]
                word_X = content_X + len(pre_rlink)
                word_color = Globals.RLINK_COLORS[line.rlink]
                if line.rlink_anchor == 1:
                    #word_value = "+{}".format(word_value)
                    word_color = word_color | curses.A_UNDERLINE
                window.addstr(last_line_y, word_X, word_value, word_color)
            last_line_y += Globals.LINE_DISTANCE
    else:
        window.addstr(Globals.HEAD_Y+6,
                      Globals.HEAD_X+30,
                      "No sections to display!", curses.A_STANDOUT)
    window.border(0)
    if MESSAGE:
        logger.debug("CLI MESSAGE: \n{}".format(MESSAGE))
        try: 
            add_multiline_text(window, last_line_y+4, Globals.PROMPT_X, MESSAGE)
        except Exception as e:
            window.clrtobot()
            window.border(0)
            ssw = ScrollSubWin(window, last_line_y+4, Globals.PROMPT_X, MESSAGE)
            ssw.run()
            window.border(0) # to remove help mess
        MESSAGE = ''
    window.refresh()

# cmd implementation
def do_cli(window, **kwargs):
    """
    Display song on window after each command. 
    !!! For the help to work with a new command, the new command has to aslo
    be entered in the help section

    :input: window - window to print song on
            kwargs: cmd - command to run
                    words_per_line - used for randomize more/less. 
                               Can be: 
                                 ''   - (empy string) will be ignorred
                                 '+'  - increment number of words for each line
                                 '-'  - decrement number of words for each line
    :returns: 
              True - if user didn't enter exit or no unexpected error occured
                     (return True will keep the program running)
              False - otherwise (which will cause the program to exit)
    """
    global last_line_y
    global prompt_y
    global redo_limit
    global MESSAGE

    # read kwargs
    cmd, words_per_line = None, None
    for key in kwargs:
        if key == 'cmd':
            cmd = kwargs[key]
        elif key == 'words_per_line':
            words_per_line = kwargs[key]

    # print prompt
    prompt_y = last_line_y + 2
    if cmd: 
        kinput = cmd
    else: 
        kinput = do_cli_prompt(window, "(snd): ", prompt_y, Globals.PROMPT_X)

    # redo/undo code - 
    #   save song data if song modified by previous command
    #   shelf_get(Globals.SONG_HISTORY[n]) will return the song state as it 
    #   was before running the Globals.CMD_HISTORY[n] command
    
    if (Globals.SONG_BEFORE_PREV_CMD 
        and Globals.PREV_CMD 
        and Globals.PREV_CMD not in Globals.SKIP_CMD_HISTORY):
        if Globals.PREV_CMD not in ['redo','.']:
            if kinput in ['redo', '.']:
                redo_limit = Globals.SHELF_KEY - 1
        if Globals.SONG_BEFORE_PREV_CMD != Globals.SONG:
            shelf_put(Globals.SHELF_KEY, Globals.SONG_BEFORE_PREV_CMD)
            logger.debug("Saving song as it was before running '{}'.".format(
                    Globals.PREV_CMD))
            logger.debug("Shelf key: '{}'.".format(Globals.SHELF_KEY))
            Globals.CMD_HISTORY.append(Globals.PREV_CMD)
            if Globals.PREV_CMD not in ['undo', 'z', 'redo', '.']:
                Globals.DOUNDO_KEY = Globals.SHELF_KEY
            Globals.SHELF_KEY += 1
        else:
            logger.debug("No song changes detected after running '{}'.".format(Globals.PREV_CMD))
    logger.debug("GOT COMMAND: '{}'".format(kinput))    
    Globals.SONG_BEFORE_PREV_CMD = deepcopy(Globals.SONG)
    Globals.PREV_CMD = kinput

    rmatch = Re()

    # do nothing on enter
    if kinput == '':
        return True
    
    # title - change song title
    elif rmatch.match('(title)( (.+))', kinput):
        title = rmatch.result.group(3)
        if title:
            if len(title) > Globals.MAX_TITLE_CHARS:
                MESSAGE = "New title must be more than 0 and less than {} chars in length.".format(Globals.MAX_TITLE_CHARS)
            else:
                MESSAGE = "Title changed to '{}'".format(title)
                Globals.SONG.name = title
        else:
            ret_code, MESSAGE = man.get_help('title')

    # help - command list    
    elif kinput in ['help', '?']:
        MESSAGE = man.get_all_summary()

    # help - command help
    elif rmatch.match('(help|\?)\ (.+)', kinput):
        command = rmatch.result.group(2)
        try:
            ret_code, MESSAGE = man.get_help(command)
        except KeyError as e:
            MESSAGE = '''Unknown command {}. 
Type '?' or 'help' for command list.'''.format(e)

    # exit
    elif kinput in ['quit', 'q', 'exit'] :
        question = "Are you sure you want to exit? (y|n): "
        comment = "WARNING: This will erase all unsaved current song data."
        response = do_warning(window, question, comment)
        if response in ['y', 'Y']:
            return False
        else:
            MESSAGE = "Quit song canceled."

    # new - start new song
    elif kinput == 'new':
        question = "Are you sure you want to start a new song? (y|n): "
        comment = "WARNING: This will erase all unsaved current song data."
        response = do_warning(window, question, comment)
        if response in ['y', 'Y']:
            Globals.START_NEW = True
        else:
            MESSAGE = "Starting new song canceled."
            

    # hist - print history list
    elif kinput in ['hist', 'history']:
        MESSAGE = Globals.CMD_HISTORY
                  
    
    # print limits
    elif kinput == 'limits':
        MESSAGE = """
    {}: {}
    {}: {}
    {}: {}
    {}: {} 
    {}: {}""".format('MAX_LINES', Globals.MAX_LINES,
                     'MAX_SYLLABLES', Globals.MAX_SYLLABLES,
                     'MAX_STRUCTURE_CHARS', Globals.MAX_STRUCTURE_CHARS,
                     'MAX_TITLE_CHARS', Globals.MAX_TITLE_CHARS,
                     'MAX_LINE_CHARS', Globals.MAX_LINE_CHARS) 

    # line space
    elif kinput in ['ls', 'linespace']:
        if Globals.LINE_DISTANCE == 2:
            Globals.LINE_DISTANCE = 1
        else:
            Globals.LINE_DISTANCE = 2

    # undo
    elif kinput in ['undo', 'z']:
        if Globals.SHELF_KEY > 0:
            if Globals.DOUNDO_KEY == -2:
                Globals.DOUNDO_KEY = Globals.SHELF_KEY - 1
        if Globals.DOUNDO_KEY >= 0:
            undo_song = shelf_get(Globals.DOUNDO_KEY)
            undo_cmd = Globals.CMD_HISTORY[Globals.DOUNDO_KEY]
            if undo_song:
                del(Globals.SONG)
                Globals.SONG = deepcopy(undo_song)
                Globals.DOUNDO_KEY -= 1
                MESSAGE = "Performed undo for '{}'".format(undo_cmd)
            else:
                MESSAGE = "ERROR: Could not perform undo. See log for details."
        else:
            MESSAGE = "No more commands to undo."

    # redo
    elif kinput in ['redo', '.']:
        # if no undo performed, redo last cmd in cmd history
        if 'redo_limit' not in globals() and Globals.CMD_HISTORY:
            last_cmd = Globals.CMD_HISTORY[-1]
            return do_cli(window, cmd=last_cmd)
        # redo the last undid cmd
        if Globals.DOUNDO_KEY >= -1 and Globals.DOUNDO_KEY < redo_limit:
            Globals.DOUNDO_KEY += 1
            redo_song = shelf_get(Globals.DOUNDO_KEY+1)
            redo_cmd = Globals.CMD_HISTORY[Globals.DOUNDO_KEY]
            if redo_song:
                del(Globals.SONG)
                Globals.SONG = deepcopy(redo_song)
                MESSAGE = "Performed redo for '{}'".format(redo_cmd)
            else:
                MESSAGE = "ERROR: Could not perform redo. See log for details."
        else:
            try:
                redo_cmd = Globals.CMD_HISTORY[Globals.SHELF_KEY-1]
                if redo_cmd:
                    do_cli(window, redo_cmd)
                    return True
            except:
                MESSAGE = "Nothing to redo."
            else:
                MESSAGE = "No more commands to redo."

    # rmode
    elif rmatch.match('(rmode)( (.+))', kinput):
        mode_no = rmatch.result.group(3)
        MESSAGE = Globals.SONG.set_rmode(mode_no)

    # ra 
    elif kinput == 'ra':
        MESSAGE = Globals.SONG.randomize_section('active', words_per_line)
    
    # +/- = ra more/less
    elif kinput in ['+', '-']:
        last_randomize_cmd = get_last_line_randomize_cmd()
        if not last_randomize_cmd:
            MESSAGE = 'WARNING: No line randomization command to redo.'
            return True
        return do_cli(window, cmd=last_randomize_cmd, words_per_line=kinput)

    # wl - wordlist 
    elif rmatch.match('(wordlist|wl)\s(.+)', kinput):
        new_ra_dir = rmatch.result.group(2)
        MESSAGE = Globals.SONG.wordlist(new_ra_dir)
        
    # jl - jumble lines
    elif rmatch.match('^(jl)\s?$|^(jl) ([a|s])$', kinput):
        option = rmatch.result.group(3)
        MESSAGE = Globals.SONG.jumble_lines(option)
    
    # up - command history
    elif kinput == 'KEY_UP':
        MESSAGE = "GOING UP"

    # down - command history
    elif kinput == 'KEY_DOWN':
        MESSAGE = "Going DOWN"

    # save
    elif rmatch.match('(save)(\s(.+))*', kinput):
        file_name = rmatch.result.group(3)
        MESSAGE = Globals.SONG.save(file_name)

    # export - save as text
    elif rmatch.match('(export)(\s(.+))*', kinput):
        file_name = rmatch.result.group(3)
        MESSAGE = Globals.SONG.save(file_name, save_as_text=True)

    # load
    elif rmatch.match('(load)(\s(.+))*', kinput):
        file_name = rmatch.result.group(3)
        MESSAGE = Globals.SONG.load(file_name)

    # rr - random random
    elif kinput == 'rr':
        return do_cli(window, cmd=random.choice(Globals.RA_COMMANDS))

    ################
    # line actions #
    ################

    # test various functions
    elif rmatch.match('(gw)( (.+))', kinput):
        syll_no = rmatch.result.group(3)
#        MESSAGE = get_random_word(syll_no)
        MESSAGE = get_syll_count(syll_no)

    # l - lock
    elif rmatch.match('(l) (\S+)( (\S+(-\S+)?))?', kinput):
        # MESSAGE = ""
        # for index, group in enumerate(rmatch.result.groups()):
        #     MESSAGE +=  "\ngr {}: {}".format(index, group)
        line_no = rmatch.result.group(2)       
        lock_range = rmatch.result.group(4)
        # MESSAGE += "\n ln = {}\n lr = {}".format(line_no, lock_range)
        MESSAGE = Globals.SONG.lock_line(line_no, lock_range)

    # u - unlock
    elif rmatch.match('(u) (\S+)( (\S+(-\S+)?))?', kinput):
        line_no = rmatch.result.group(2)       
        lock_range = rmatch.result.group(4)
        MESSAGE = Globals.SONG.unlock_line(line_no, lock_range)

    # rl - randomize line
    elif rmatch.match('(rl$)|(rl\s(.+))', kinput):
        line_no = rmatch.result.group(3)
        MESSAGE = Globals.SONG.randomize_line(line_no, words_per_line)

    # al - add line
    elif rmatch.match('(al)( (.+))', kinput):
        line_no = rmatch.result.group(3)
        MESSAGE = Globals.SONG.add_line(line_no, randomize=True)

    # ael - add empty line
    elif rmatch.match('(ael)( (.+))', kinput):
        line_no = rmatch.result.group(3)
        MESSAGE = Globals.SONG.add_line(line_no)

    # dl - delete line
    elif rmatch.match('(dl)( (.+))', kinput):
        line_no = rmatch.result.group(3)
        MESSAGE = Globals.SONG.del_line(line_no)

    # cl - copy line
    elif rmatch.match('(cl)( (.+))', kinput):
        line_no = rmatch.result.group(3)
        MESSAGE = Globals.SONG.copy_line(line_no)

    # pl - paste line
    elif rmatch.match('(pl)( (.+))', kinput):
        line_no = rmatch.result.group(3)
        MESSAGE = Globals.SONG.paste_line(line_no)

    # ml - move line
    elif rmatch.match('(ml)(\s(\d+)\s?([+|-]))', kinput):
        line_no = rmatch.result.group(3)
        direction = rmatch.result.group(4)
        MESSAGE = Globals.SONG.move_line(line_no, direction)

    # e - edit line 
    elif rmatch.match('^e (.+)$', kinput) or rmatch.match('^(\d{,2})$', kinput):
        initial_line_distance = Globals.LINE_DISTANCE
        if initial_line_distance == 1:
            do_cli(window, cmd='linespace')
            print_screen(window)
            do_cli(window, cmd=kinput)
            Globals.LINE_DISTANCE = initial_line_distance
            return True
        line_no = rmatch.result.group(1)
        ret_code, MESSAGE = Globals.SONG.check_line_no(line_no, check_max=False)
        if MESSAGE == '':
            line_idx = int(line_no) - 1
            line_y = Globals.FIRST_LINE_Y + Globals.LINE_DISTANCE * line_idx
            initial_line_content = Globals.SONG.get_line_contents(line_idx)
            # show line distance to make all lines visible during edit
            new_line_content = show_editbox(window, 1, Globals.MAX_LINE_CHARS, 
                                            line_y, Globals.LINE_X, 
                                            initial_text=initial_line_content)
            if Globals.EDIT_EXIT_KEY == 'UP':
                MESSAGE = Globals.SONG.set_line_content(line_idx, new_line_content)
                line_no = 1
                if line_idx-1 >= 0:
                    line_no = line_idx
                print_screen(window)
                return do_cli(window, cmd='e {}'.format(line_no))
            elif Globals.EDIT_EXIT_KEY == 'DOWN':
                MESSAGE = Globals.SONG.set_line_content(line_idx, new_line_content)
                line_no = Globals.SONG.get_active_section_size()
                if line_idx+1 < line_no:
                    line_no = line_idx+2
                print_screen(window)
                return do_cli(window, cmd='e {}'.format(line_no))
            elif Globals.EDIT_EXIT_KEY == 'ESC':
                MESSAGE = "Line {} was not changed.".format(line_idx+1)
            else:
                MESSAGE = Globals.SONG.set_line_content(line_idx, new_line_content)
     
    # c - clear line
    elif rmatch.match('(c|clear)\s(.+)', kinput):
        line_no = rmatch.result.group(2)
        MESSAGE = Globals.SONG.clear_line(line_no)

    # lo - line on/off 
    elif rmatch.match('lo(\s([^\s\*]+)$|(\s\*)(\s([x|X]))?|((\s\S+){2,}))$', kinput):
        line_no = rmatch.result.group(1).strip()
        state = rmatch.result.group(5)
        if state:
            line_no = line_no[0]
        MESSAGE = Globals.SONG.switch_line_state(line_no, state)

    # slg - syllable line guide
    elif rmatch.match('(slg)(\s(\*|\d+)\s(\d+))', kinput):
        line_no = rmatch.result.group(3)
        sylls_no = int(rmatch.result.group(4))
        MESSAGE = Globals.SONG.set_syllable_guide(line_no, sylls_no)


    ###################
    # section actions #
    ###################

    # as - add section
    elif rmatch.match('(as)(\s(.+))', kinput):
        section_letter = rmatch.result.group(3).upper()
        MESSAGE = Globals.SONG.add_new_section(section_letter)

    # ds - delete section
    elif kinput == 'ds':
        MESSAGE = Globals.SONG.del_active_section()
        
    # n - next section
    elif kinput in ['n', 'KEY_RIGHT']:
        MESSAGE = Globals.SONG.set_active_section('next')[1]

    # b - previous section
    elif kinput in ['b', 'KEY_LEFT']:
        MESSAGE = Globals.SONG.set_active_section('prev')[1]

    # go - preview entire song in one screen
    elif kinput == 'go':
        song_text = Globals.SONG.get_full_text()
        show_text(window, song_text)

    # cs - copy section
    elif kinput == 'cs':
        MESSAGE = Globals.SONG.copy_active_section()

    # ps - paste section
    elif kinput == 'ps':
        MESSAGE = Globals.SONG.paste_section()

    # ms - move section
    elif rmatch.match('(ms)(\s?([+|-]))', kinput):
        direction = rmatch.result.group(3)
        MESSAGE = Globals.SONG.move_active_section(direction)

   # make - change section type
    elif rmatch.match('((make)\s(.)(\+?))$', kinput):
        section_letter = rmatch.result.group(3).upper()
        ovrt_modifier = rmatch.result.group(4)
        MESSAGE = Globals.SONG.make_active_section(section_letter, ovrt_modifier)

    # substitute
    elif rmatch.match('s/([^/]*)/([^/]*)(/\w*)?', kinput):
        s_from = rmatch.result.group(1)
        if not s_from:
            MESSAGE = "Notihing to replace..."
        else:
            s_to = rmatch.result.group(2) 
            if not s_to: s_to = ''
            g_switch = rmatch.result.group(3)
            global_replace = 0
            if g_switch == '/g':
                global_replace = 1
            MESSAGE = Globals.SONG.replace(s_from, s_to, global_replace)

    # rlink - create/delete rhyme links
    elif rmatch.match('^(rlink|r)\s(\S)((\s\S{1,})+)$', kinput):
        modif = rmatch.result.group(2)
        lines = rmatch.result.group(3).strip().split()
        MESSAGE = Globals.SONG.rlink(modif, *lines)

    # show - show various info needed
    elif rmatch.match('^(show|sh)\s(\w+)$', kinput):
        modif = rmatch.result.group(2)
        MESSAGE = Globals.SONG.show(modif)

    # hide
    elif rmatch.match('^(hide|h)\s(\w+)$', kinput):
        modif = rmatch.result.group(2)
        MESSAGE = Globals.SONG.hide(modif)

    # ln - show/hide line numbers
    elif kinput == 'ln':
        MESSAGE = Globals.SONG.switch_line_numbers()

    ###########    
    # default #
    ###########    
    else:
        command = kinput.split(' ')[0]
        ret_code, MESSAGE = man.get_help(command)
    
    return True

