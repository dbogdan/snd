
from line import Line
from snd_tui.lib import *
from snd_conf import logger, Globals
from copy import deepcopy

class Section(object):

    def __init__(self, section_info, section_type, active=0):
        self.line_count = int(section_info[0])
        self.syll_count = int(section_info[1])
        self.type = section_type
        self.is_active = active
        # generate lines
        self.lines = [Line(syllables=self.syll_count, 
                           content="Section {} Line {} ... ... ...".format(self.type, line_no+1),
                           index=line_no)
                      for line_no in range(self.line_count)]
        # list of rlink values (ints marking rlinked lines)
        # each line will be marked with a rlink value; if two 
        # lines have the same rlink value, they are rlinked together  
        # E.g.: [[] [1,3] [2,4]] - lines 1 and 3 are rlinked with rlink no. 1
        #                        - lines 2 and 4 are rlinked by rlink no. 2
        self.rlinks = [0] # 0 = no rlink
  
        
    def __eq__(self, other):
        if (self.line_count == other.line_count
            and self.syll_count == other.syll_count
            and self.type == other.type
            and self.is_active == other.is_active
            and self.lines == other.lines):
            return True
        return False

    def __ne__(self, other):
        if (self.line_count != other.line_count
            or self.syll_count != other.syll_count
            or self.type != other.type
            or self.is_active != other.is_active
            or self.lines != other.lines):
            return True
        return False

    def get_type(self):
        return self.type

    def set_type(self, new_type):
        self.type = new_type

    def show_raw_lines(self):
        print self.lines

    def get_syll_count(self):
        return self.syll_count

    def set_syll_count(self, no):
        if no <= Globals.MAX_SYLLABLES:
            self.syll_count = no

    def set_active(self):
        self.is_active = 1

    def unset_active(self):
        self.is_active = 0  
        
    def replace(self, from_text, to_text):
        """
        Replace from_text with to_text in line contents section wide.
        """
        for line in self.lines:
            ret_mess = line.replace(from_text, to_text)
            if ret_mess: return ret_mess

    def lock_line(self, line_no, lock_range):
        line = self.lines[line_no]
        if lock_range:
            if line.lock_words(lock_range) == 0:
                return "Word(s) {} from line {} locked.".format(lock_range, line_no+1)
            else:
                return "ERROR:Invalid word number or word range start number."
        else:
            line.lock()
            return "Line {} locked.".format(line_no+1)
        
    def unlock_line(self, line_no, lock_range):
        line = self.lines[line_no]
        if lock_range:
            if line.unlock_words(lock_range) == 0:
                return "Word(s) {} from line {} unlocked.".format(lock_range, line_no+1)
            else:
                return "ERROR:Invalid word number or word range start number."            
        else:
            line.unlock()
            return "Line {} unlocked.".format(line_no+1)

    def del_line(self, line_no=-1):
        """
        Delete line on position line_no. 

        :type line_no: int 
        :param line_no: Optional parameter that specifies position of line to delete. 
                    If negative or not specified, last line will be deleted.
        """
        if line_no < 0 or line_no == '':
            line_no = -1
        del self.lines[int(line_no)]
        self.update_line_count()
        self.update_line_numbers()

    def add_line(self, line_no=-1, randomize=False, new_line=None):
        """ 
        Add a new empty line on position line_no.
        
        :type line_no: int 
        :param line_no: Optional parameter that specifies position to insert line at. 
                    If negative or not specified, line will be added at the end.
                    Zero indexed.
        :type randomize: boolean            
        :param randomize: Optional parameter. If not False, line will be filled
                    with random content.
        :type line: Line
        :param line: Line object instance to add to active section.
        """ 
        if self.line_count == Globals.MAX_LINES:
            return "ERROR: Section reached maximum number of lines ().".format(self.line_count)
        if not new_line:
            new_line = Line(self.syll_count)
        else:
            new_line = deepcopy(new_line)
        new_line.index = line_no
        if randomize:
            new_line.randomize()
        if line_no < 0 or line_no == '':
            self.lines.append(new_line)    
        else:
            self.lines.insert(int(line_no), new_line)
        self.update_line_count()
        self.update_line_numbers()
        return 'Added line on position {}. Total lines: {}'.format(line_no+1,
                                                                   self.line_count)

    def set_syllable_guide(self, line_no, sylls_no):
        self.lines[line_no].required_syll_count = sylls_no

        
    def randomize_lines(self, words_per_line=''):
        """
        :type words_per_line: str
        :param words_per_line: Can be:
                                 ''   - (empy string) will be ignorred
                                 '+'  - increment number of words for each line
                                 '-'  - decrement number of words for each line
        :rtype: str
        :return: Empty string on success and error message on error
        """
        # make sure randomize path exists
        if not os.path.exists(Globals.RA_PATH):
            message = "ERROR: Randomize dir does not exist."
            logger.debug(message)
            return message

        # make sure we have lines 
        if self.line_count == 0:
            return "Nothing to randomize. There are no lines present."

        # make sure we have lines in ON mode
        unlocked_lines = [line for line in self.lines if not line.is_locked]
        if len(unlocked_lines) == 0:
            return "Nothing to randomize. All lines are locked."

        # make sure we have enabled lines 
        enabled_lines = [line for line in self.lines if line.is_enabled]
        if len(enabled_lines) == 0:
            return "Nothing to randomize. All lines are disabled."
        
        message = ''
        # get refference words for each rlink
        reff_words = dict()
        logger.debug('getting_ref words')
        for line in self.lines:
            logger.debug('Line: {}'.format(line.get_content()))
            logger.debug('\trlink: {}, anchor: {}'.format(line.rlink, line.rlink_anchor))
            if line.rlink_anchor == 1:
                for word in line.words:
                    logger.debug('Word: {}, anchor: {}'.format(word.get_value(),
                                                               word.rlink_anchor))
                    if word.rlink_anchor == 1:
                        reff_words[line.rlink] = word.get_value() 
            logger.debug('Dict: {}'.format(reff_words))
        # randomize lines
        for line_index, line in enumerate(self.lines):
            line.set_index(line_index)
            ret_code, ret_mess = line.randomize(words_per_line)
            if ret_code != 0:
                if ret_code == 1:
                    message += "{}\n".format(ret_mess)
                else:
                    return "{}\n\t{}".format(ret_mess, "See log for details.")
            if line.rlink != 0 \
                    and not line.is_locked \
                    and line.is_enabled \
                    and not line.words[line.rlink_word_idx].is_locked:
                logger.debug("Rlinked word index: {}".format(line.rlink_word_idx))
                logger.debug("REFF_WORDS: {}".format(reff_words))
                line.set_word_value(line.rlink_word_idx, 
                                    get_rhyme(reff_words[line.rlink]))
                logger.debug("Final content: '{}'".format(line.get_content()))
        return message

    def get_rlink_list(self):
        """
        Return a list containing on each position another list consisting
        of the indexes of the lines that have rlink set to the position value

        Ex: [[] [1,3] [2,4] []] - lines 1 and 3 have the rlink value set to 1
                                - lines 2 and 4 have the rlink value set to 2
        """
        rlink_list = [[] for x in range(Globals.MAX_LINES/2+1)]
        for line in self.lines:
            rlink_val = line.rlink
            if rlink_val != 0:
                rlink_list[rlink_val].append(line.index)
        return rlink_list


    def create_rlink(self, *line_indexes):
        """
        Create rlink between the lines specified by the line_indexes 
        (use * idiom to allow arbitrary number of lines)
        """
        # remove duplicates but keep the order
        # line_indexes = list(set(line_indexes))
        uniq_indexes = list()
        for index in line_indexes:
            if index not in uniq_indexes:
                uniq_indexes.append(index)
        if len(uniq_indexes) < 2:
            return "ERROR: We need at least two lines to create a rlink."
        new_rlink = self.rlinks[-1]+1
        for line_index in uniq_indexes:
            line, word = line_index
            err_code, err_mess = self.lines[line].set_rlink(new_rlink, word)
            if err_code:
                return err_mess
        self.rlinks.append(new_rlink)
        self.lines[uniq_indexes[0][0]].set_rlink_anchor()
        return "Created rlink between lines {}".format([line[0] + 1 for line in uniq_indexes])
        

    def delete_rlink(self, *line_indexes):
        """
        Delete rlinks on the specified lines and all lines 
        rlinked with them.
        """
        del_rlinks = set([self.lines[line[0]].rlink for line in line_indexes 
                     if self.lines[line[0]].rlink != 0])
        if not del_rlinks:
            return "No rlinks found on specified lines."
        # delete rlinks
        for line_index in line_indexes:
            line = self.lines[line_index[0]]
            if line.rlink in del_rlinks:
                line.unset_rlink(line_index[1])
                if line.rlink_anchor:
                    line.unset_rlink_anchor()
        # reset rlink anchors
        for rlink_value in del_rlinks:
            rlink_list = self.get_rlink_list()
            try:
                anchor_line_index = rlink_list[rlink_value][0]
                self.lines[anchor_line_index].set_rlink_anchor()
            except IndexError:
                continue
        return "Deleted rlinks from lines {}.".format([line[0] + 1 for line in line_indexes])

    def get_progress(self):
        locked_sylls = 0
        unlocked_sylls = 0
        for line in self.lines:
            locked, unlocked = line.get_progress()
            locked_sylls += locked
            unlocked_sylls += unlocked
        return locked_sylls, unlocked_sylls

    def update_line_numbers(self):
        for index, line in enumerate(self.lines):
            line.set_index(index)

    def update_line_count(self):
        self.line_count = len(self.lines)

