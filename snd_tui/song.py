from snd_tui.lib import *
from snd_conf import logger, Globals
from section import Section
from line import Line
from copy import deepcopy
try:
    import cPickle as pickle
except:
    import pickle
from random import shuffle
from time import time
import os
import fnmatch


class Song(object):

    def __init__(self, song_info):
        self.song_info = song_info
        self.name = self.song_info['name']
        self.initial_structure = self.song_info['structure']
        # generate sections
        self.sections = []
        for section_initial in self.initial_structure:
            section_type = get_section_from_initial(section_initial)
            self.sections.append(Section(self.song_info[section_type+"_specs"], 
                                         section_type))
        # set first section as active
        self.sections[0].set_active()
        # add generic line and section (to avoid ne and eq errors)
        self.line_clipboard = Line()
        self.section_clipboard = Section(('0','0'), 'Generic')
        self.show_rlinks = True

    def __eq__(self, other):
        if (self.name == other.name 
            and self.sections == other.sections
            and self.line_clipboard == other.line_clipboard
            and self.section_clipboard == other.section_clipboard): 
            return True
        return False

    def __ne__(self, other):
        if (self.name != other.name
            or self.sections != other.sections
            or self.line_clipboard != other.line_clipboard
            or self.section_clipboard != other.section_clipboard): 
            return True
        return False

    def set_name(name):
        self.name = name

    def get_active_section_index(self):
        """
        Return index of active section
        """
        if not self.sections:
            return 0
        for section in self.sections: 
            if section.is_active:
                return self.sections.index(section)
        
    def get_active_section(self):
        """
        Get active section information 

        :rtype: Section
        :return: section object
        """
        for section in self.sections: 
            if section.is_active:
                return section
            
    def get_active_section_type(self):
        active_section = self.get_active_section()
        if active_section: 
            return active_section.get_type()
        else:
            return 'Xxxx'

    def get_active_section_type_initial(self):
        return get_section_initial_from_type(self.get_active_section_type())

            
    def get_active_section_size(self):
        """
        Return number of lines in active section

        :trype: int
        """
        active_section = self.get_active_section()
        return len(active_section.lines)

    def add_new_section(self, new_section, position=-1):
        """
        Add a new section before the active section or at specified position.

        :type new_section: str or Section
        :param new_section: 
                   If string: Letter describing section type:
                                  'V' = Verse  
                                  'C' = Chorus     
                                  'B' = Bridge 
                                  'M' = Middle 8
                   If Section - new section object to add   

        :type position: int 
        :param position: Optional argument.
                         Default value: -1
                         Position to copy section at (0 index).
                         If -1 or > len(self.sections) will be ignorred and
                         section will be added before current section.

        :rtype: str
        :return: One line message that will be printed to t
        """
        # don't fail and set new section to active if there are no sections in place
        if self.sections:
            # make sure new section can be added
            if len(self.sections) == Globals.MAX_STRUCTURE_CHARS:
                return "WARNING: Maximum sections number reached. Section not added."
            active_sindex = self.get_active_section_index()
            if -1 < position <= len(self.sections):
                new_sindex = position
            else:
                new_sindex = active_sindex #add before active index 
            set_active = 1
        else:
            new_sindex = 0
            set_active = 1
        # if we have a section letter as parameter
        if type(new_section) is str:
            # check specified section type
            if new_section not in Globals.VALID_SECTION_INITIALS:
                return "ERROR: Section structure type not valid. See help for details."
            # use default values for lines and syllables if none specified at welcome
            new_stype = get_section_from_initial(new_section)
            new_sinfo_key = new_stype+"_specs"
            try:
                whatever = self.song_info[new_sinfo_key]
            except KeyError:
                self.song_info[new_sinfo_key] = (Globals.DEFAULTS[new_stype+"_lines"],
                                                 Globals.DEFAULTS[new_stype+"_syllables"])
            # create section object
            new_section_inst = Section(self.song_info[new_sinfo_key], 
                                       new_stype, 
                                       set_active) 
        # if we have a section instance as parameter
        elif type(new_section) is Section:
            new_type = new_section.type
            new_section_inst = new_section
        else:
            return "Unknown type {} for new section {}".format(type(new_section), 
                                                               new_section)
        # add section to sections list
        init_sections_no = len(self.sections)
        if set_active == 1:
            if self.sections:
                self.sections[active_sindex].unset_active()
            new_section_inst.set_active()
        self.sections.insert(new_sindex, new_section_inst)
        return "New section successfuly added."
            
    def copy_active_section(self):
        """
        Copy active section to clipboard
        """
        if self.sections:       
            self.section_clipboard = deepcopy(self.get_active_section())
            return "Active section copied."
        return "No sections to copy..."

    def paste_section(self, position=-1):
        """
        Paste previousely copied section before currently active section or
        on specified postion and make it the active section.

        :type position: int 
        :param position: Optional argument.
                         Default value: -1
                         Position to copy section at (0 index).
                         If -1 or >= len(self.sections) will be ignorred and
                         section will be copied after current section
        """
        if self.section_clipboard and self.section_clipboard.type != 'Generic':
            new_section_inst = deepcopy(self.section_clipboard)
            message = self.add_new_section(new_section_inst, position)
        else:
            message = "ERROR: No section in clipboard. Use 'cs' to copy active section."
        return message

    def del_active_section(self):
        """
        Delete active session.
        If not deleting last section, sets next section as active.
        Else sets previous section as active.
        """
        if not self.sections:
            return "No more sections to delete."
        active_sindex = self.get_active_section_index()

        # don't mess up if active section is last
        if self.set_active_section('next')[0] != 0:
            self.set_active_section('prev')
            
        # del from section instances dict
        del self.sections[active_sindex]
        return "Section deleted."
                                       
    def move_active_section(self, direction='up'):
        """
        Move section  up or down

        :type direction: str
        :param direction: Accepted values:
                           - 'up' or '+' - move section one position up (right)
                           - 'dn' or '-' - move section one position down (left)
        :rtype: str
        :return: Message confirming line move
        """
        if not self.sections:
            return "Please add a section first."
        active_sindex = self.get_active_section_index()
        if direction in ['up', '+']:
            if active_sindex == len(self.sections)-1:
                return "Last section cannot be moved lower (right)."
            to_index = active_sindex + 1
        elif direction in ['dn', '-']:
            if active_sindex == 0:
                return "First section cannot be moved higher (left)."
            to_index = active_sindex - 1
        else:
            return "ERROR: Unknown direction '{}'".format(direction)
        m1 = self.copy_active_section()
        m2 = self.del_active_section()
        m3 = self.add_new_section(self.section_clipboard, to_index)
        return "Section moved."

    def make_active_section(self, new_section_type, ovrt_modifier):
        """
        :type new_section: str 
        :param new_section: Letter describing section type:
                                  'V' = Verse  
                                  'C' = Chorus     
                                  'B' = Bridge 
                                  'M' = Middle 8
        :type ovrt_modifier: str
        :param ovrt_modifier: If specified and equal to "+", will overvrite all 
                    sections of type "new_section" with the current section 

         For example:
             make C < will make the current section in to a Chorus section
             make C+ < will make the current section in to a Chorus section and 
                      overwrite all other Chorus sections with the current section 
        """
        # check specified section type
        if new_section_type not in Globals.VALID_SECTION_INITIALS:
            return "ERROR: Section structure type not valid. See help for details."
        active_section = self.get_active_section()
        old_type = active_section.get_type()
        active_section.set_type(get_section_from_initial(new_section_type))
        active_type = active_section.get_type()
        message =  "Active section type changed from '{}' to '{}'".format(
            old_type, active_type)
        if ovrt_modifier == '+':
            old_sections = self.sections
            self.sections = []
            for section in old_sections:
                if section.is_active == 0 and section.get_type() == active_type:
                    section = deepcopy(active_section)
                    section.unset_active()
                self.sections.append(section)
            message += "\nAll sections of type {} replaced with active section".format(active_type)
        return message

    def jumble_lines(self, section=None):
        """
        Jumble lines of specified section.

        :type section: str 
        :param section: -optional argument - accepded values:
                                - None - jumble lines in active section
                                - 's' - jumble lines for all sections
                                                with same type as active section
                                - 'a' - jumble lines in all sections
        """
        if not self.sections:
            return "No section left to jumble."
        message = ""
        if section == None:
            active_section = self.get_active_section()
            if active_section.line_count <= 0:
                return "Section {}: No lines to jumble...".format(active_section.type)
            sections = [active_section]
            message = "Section {}: Lines jumbled.".format(active_section.type)
        elif section == 'a':
            sections = [section for section in self.sections]
            active_type = "all"
            message = "Song lines jumbled."
        elif section == 's':
            active_type = self.get_active_section_type()
            sections = [section for section in self.sections 
                        if section.type == active_type]
            message = "{} lines jumbled".format(active_type.capitalize())

        else:
            return "jumble_lines: Unknown value for section arg:\t'{}'".format(section)

        # handle jumble lines for multiple sections (options s and a)
        lines = [line.get_content() for section in sections 
                 for line in section.lines if line.is_enabled]
        shuffle(lines)
        for section in sections:
            if section.line_count:
                for line in section.lines:
                    if line.is_enabled:
                        logger.debug("Lines in for: \n{}".format(lines))
                        jline_content = lines[0]
                        line.set_content(jline_content)
                        del lines[0]
        return message

    def set_rmode(self, mode_no):
        """
        Set the randomize mode (source).
        :type mode_no: str
        :param mode_no: Desired randomize mode. 
             Accepted values:
                   '1' - uses dictionary (like it does currently)
                   '2' - use only syllable files
                   '3' - does a 50% dictionary and 50% syllable files propagation
                         of required syllables in a round robin dict, syllable files, 
                         dict, syllable files, dict, syllable files, dict, etc.
        """
        mode_no = int(mode_no)
        if mode_no not in Globals.VALID_RMODES:
            return "ERROR: Invalid mode '{}'.".format(mode_no)
        Globals.RMODE = mode_no
        return "Activated randomize mode {}.".format(Globals.RMODE)


    def randomize_section(self, section='active', words_per_line=''):
        """
        Randomize specified section using the files in Globals.RA_PATH

        :type section: str 
        :param section: -optional argument - if set to 'active' will randomize 
                                    active section

        :type words_per_line: str
        :param words_per_line: Can be:
                                 ''   - (empy string) will be ignorred
                                 '+'  - increment number of words for each line
                                 '-'  - decrement number of words for each line
        """
        if not self.sections:
            return "No section left to randomize."
        logger.debug("Running ra on section '{}'.\n\tRandomize path: {}".format(
                section, Globals.RA_PATH))
        if section == 'active':
            active_section = self.get_active_section()
            message = active_section.randomize_lines(words_per_line)
            if message == '': 
                message = "Randomized section."
        return message    


    def randomize_line(self, line_no, words_per_line=''):
        """
        Randomize line from active section
        
        :type line_no: int 
        :param line_no: Mandatory parameter that specifies position of line to 
                        randomize.
        :type words_per_line: int
        :param words_per_line: Can be:
                                 ''   - (empy string) will be ignorred
                                 '+'  - increment number of words for each line
                                 '-'  - decrement number of words for each line
        """
        active_section = self.get_active_section()
        # choose random line if no line specified
        if not line_no:
            line_no = random.choice(range(active_section.line_count)) + 1
        # randomize line
        ret_code, message = self.check_line_no(line_no, check_max=False)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            line = active_section.lines[line_idx]
            line.set_index(line_idx) 
            code, message = line.randomize(words_per_line)
            logger.debug("randomize_line= {}".format(message))
        return message


    def lock_line(self, line_no, lock_range=None):
        ret_code, message = self.check_line_no(line_no, check_max=False)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            active_section = self.get_active_section()
            message = active_section.lock_line(line_idx, lock_range)
        return message

    def unlock_line(self, line_no, lock_range=None):
        ret_code, message = self.check_line_no(line_no, check_max=False)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            active_section = self.get_active_section()
            message = active_section.unlock_line(line_idx, lock_range)
        return message

    def get_line_contents(self, line_no):
        """
        Get contents of a line
        
        :type line_no: int
        :param line_no: number of line to read (0 is the first line)
        """
        active_section = self.get_active_section() 
        return active_section.lines[line_no].get_content()

    def get_full_text(self):
        """
        Return full song content (title and content of all lines)

        :rtype: str
        :return: buffer representing song content ready for printing 
                 (song title and line content joined with '\n')
        """
        full_text = '\t{}\n\n\n'.format(self.name)
        for section in self.sections:
            content_l = [line.get_content() for line in section.lines]
            full_text += '\n'.join(content_l)
            full_text += '\n' * 2
        return full_text

    def check_line_no(self, line_no, check_max=False, margin=1, word_no=''):
        """
        Check the specified line number is valid.
        
        :type line_no: int/str
        :param line_no: line number as specified by user 

        :type check_max: boolean
        :param check_max: if True, function will check if maximum number of lines
                          was reached and return and error message if so
                          (True val only needed for add line functions)
        
        :type margin: int
        :param margin: needed when adding a new line after the last line

        :rtype: (int, str)
        :return: Tuple of a int code and a string detailing error or empty string
                 -1 = maximum number of lines reached
                 -2 = specified line number is not a positive int
                 -3 = no sections present in the song
                 -4 = word range invalid
                  0 = OK
        """
        # make sure we have at least one section:
        if not self.sections:
            return (-3, "You need to add a section before running that command.")
        # check line
        max_line_no = self.get_active_section_size() + margin
        if not check_positive_int(line_no, max_line_no):
            return (-2, "ERROR: Line number ({}) must a positive, lower than {}, integer.".format(
                    line_no, max_line_no))
        # check word
        if word_no != "":
            max_word_no = self.get_active_section().lines[int(line_no)-1].word_count
            if not check_positive_int(word_no, max_word_no):
                return (-2, "ERROR: Word number ({}) must a positive, lower than {}, integer.".format(
                        word_no, max_word_no))
        return (0, "")


    def add_line(self, line_no, randomize=False):
        """ 
        Add a new empty line
        
        :type line_no: int 
        :param line_no: Mandatory parameter that specifies position to insert line at.
              Value comes from user. So 1 = first line, 2 = second line, etc.
        :type randomize: boolean            
        :param randomize: Optional parameter. If not False, line will be filled
                    with random content.
        """
        ret_code, message = self.check_line_no(line_no, check_max=True)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            active_section = self.get_active_section() 
            message = active_section.add_line(line_idx, randomize)
        return message

    def del_line(self, line_no):
        """
        Delete line from active section

        :type line_no: int
        :param line_no: Optional parameter that specifies position of line to delete. 
        """
        ret_code, message = self.check_line_no(line_no, check_max=False)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            active_section =  self.get_active_section() 
            active_section.del_line(line_idx)
            message = "Deleted line on position {}.".format(line_no)
        return message

    def copy_line(self, line_no):
        """
        Copy line from active section

        :type line_no: int 
        :param line_no: Mandatory parameter that specifies position of 
                        line to copy. 
        """
        ret_code, message = self.check_line_no(line_no, check_max=False)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            active_section = self.get_active_section() 
            self.line_clipboard = deepcopy(active_section.lines[line_idx])
            message = "Copied line on pos {}".format(line_no)
        return message

    def paste_line(self, line_no):
        """
        Paste previousely copied line to active section

        :type line_no: int 
        :param line_no: Mandatory parameter that specifies position to copy 
                        the line at. 
        """
        ret_code, message = self.check_line_no(line_no, check_max=False)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            active_section =  self.get_active_section()
            if self.line_clipboard:
                message = active_section.add_line(line_no=line_idx, 
                                                  new_line=self.line_clipboard)
            else:
                message = "ERROR: No line in clipboard. Use 'cl' to copy line."
        return message
    
    def move_line(self, line_no, direction='up'):
        """
        Move line up or down

        :type line_no: int 
        :param line_no: Mandatory parameter that specifies position of the line to move.

        :type direction: str
        :param direction: Accepted values - 'up' or '+'- move line one position up
                                          - 'dn' or '-'- move line one line_notiion down
        :rtype: str
        :return: Message confirming line move
        """
        ret_code, message = self.check_line_no(line_no, check_max=False)
        if ret_code == 0:
            line_idx = int(line_no) - 1
            active_section =  self.get_active_section() 
            if direction in ['dn', '-']:
                if line_idx == self.get_active_section_size() - 1:
                    return "Last line cannot be moved down."
                to_line_idx = line_idx + 1
            elif direction in ['up', '+']:
                if line_idx == 0:
                    return "First line cannot be moved up."
                to_line_idx = line_idx - 1
            line = active_section.lines.pop(line_idx)
            active_section.lines.insert(to_line_idx, line)
            message = "Line {} moved to position {}.".format(line_no, to_line_idx+1)
        return message


    def clear_line(self, line_no):
        if not self.sections:
            return "Song has no sections..."
        active_section =  self.get_active_section()
        if line_no == '*':
            for line in active_section.lines:
                line.clear()
            return "All lines cleared."
        else:
            ret_code, message = self.check_line_no(line_no, check_max=False)
            if ret_code == 0:
                line_idx = int(line_no) - 1
                active_section.lines[line_idx].clear()
                return "Line {} cleared.".format(line_no)
            return message


    def set_syllable_guide(self, line_no, sylls_no):
        """
        Change the syllable guide number for a specific line

        :type line_no: int or '*' 
        :param line_no: Mandatory parameter - position of the line to modify.
                      '*' will modify all lines.
        :type sylls_no: int
        :param sylls_no: Mandatory parameter - syllables number to set.
        """
        if not check_positive_int(sylls_no, Globals.MAX_SYLLABLES+1):
           return  "ERROR: Syllables number must a positive, lower than {}, integer.".format(Globals.MAX_SYLLABLES)
        if not self.sections:
            return "Song has no sections..."
        active_section =  self.get_active_section()
        if line_no == '*':
            for line in active_section.lines:
                line.required_syll_count = sylls_no
            return "Syllable guide number set to {} for all lines.".format(sylls_no)
        else:
            ret_code, message = self.check_line_no(line_no, check_max=False)
            if ret_code == 0:
                line_idx = int(line_no) - 1
                active_section.set_syllable_guide(line_idx, sylls_no)
                return "Syllable guide number set to {} for line {}.".format(sylls_no,line_no)
            return message
                        
    def set_line_content(self, line_idx, text):
        """
        Update a line in the active section
        
        :type line_idx: int
        :param line_no: number of line to update (0 is the first line)

        :type text: str
        :param text: text to update line with
        """
        active_section = self.get_active_section() 
        line = active_section.lines[line_idx]
        text = text.strip()
        if line.get_content() == text:
            return "Line {} was not changed.".format(line_idx+1)
        line.set_content(text)   
        return "Line {} was changed.".format(line_idx+1)

    def switch_line_numbers(self):
        """
        Turn line numbers on if off and vice-versa.
        """
        if Globals.SHOW_LINE_NUMBERS:
            Globals.SHOW_LINE_NUMBERS = False
            return "Show line numbers disabled."
        else:
            Globals.SHOW_LINE_NUMBERS = True
            return "Show line numbers enabled."

    def switch_line_state(self, line_identifier, state=''):
        """
        Switch the state of a line (disable if enabled and vice-versa)
       
        :type line_identifier: int
        :param line_identifier: number of line to update (0 is the first line)

        :type state: str
        :param state: Only used with line_identifier = '*' (all lines). 
                      Can take values:
                           'X' or 'x'    - turn all lines in active section ON
                           anything else - turn all lines in active section OFF
        """
        active_section = self.get_active_section()
        if line_identifier == '*':
            if type(state) is not str: 
                state = '' 
            if state.lower() == 'x':
                for line in active_section.lines:
                    line.enable()
                return "All lines in current section enabled."
            else:
                for line in active_section.lines:
                    line.disable()
                return "All lines in current section disabled."
        message = ''
        e_lines = []
        d_lines = []
        for line_no in set(line_identifier.split()):
            ret_code, ret_mess = self.check_line_no(line_no, check_max=False)
            if ret_code == 0:
                line_idx = int(line_no) - 1
                line = active_section.lines[line_idx]
                if line.is_enabled:
                    line.disable()
                    logger.debug("Line {} was disabled (will not be affected by randomization)".format(line_no))
                    d_lines.append(line_no)
                else:
                    line.enable()
                    logger.debug("Line {} was enabled (will not be affected by randomization)".format(line_no))
                    e_lines.append(line_no)
            else:
                message += "{}\n".format(ret_mess)
        if e_lines: message += "Line(s) {} enabled.\n".format(", ".join(e_lines))
        if d_lines: message += "Line(s) {} disabled.\n".format(", ".join(d_lines))
        return message


    def rlink(self, modif, *lines):
        active_section = self.get_active_section()
        if not active_section:
            return "Add a section first."
        line_indexes = list()
        logger.debug("rlink Lines: {}".format(lines))
        for line_info in lines:
            logger.debug("rlink line_info: {}".format(line_info))
            line_word = [idx for idx in line_info.split(':')]
            if len(line_word) == 2:
                try:
                    line_no, word_no = line_word
                    line_index = (int(line_no)-1, int(word_no)-1)
                except ValueError:
                    return "ERROR: Invalid line and/or word specified: `{}`".format(line_info)
            elif len(line_word) == 1:
                try:
                    line_no, word_no = int(line_info), ''
                    line_index = (line_no-1, word_no)
                except ValueError:
                    return "ERROR: Invalid line and/or word specified: `{}`".format(line_info)              
            else:
                return "ERROR: Invalid line and/or word specified: `{}`".format(line_info)
            ret_code, ret_mess = self.check_line_no(line_no, False, 1, word_no)
            if ret_code == 0:
                    line_indexes.append(line_index)
            else:
                return ret_mess
        if modif == 'c':
            return active_section.create_rlink(*line_indexes)
        elif modif == 'd':
            return active_section.delete_rlink(*line_indexes)
        else:
            return "ERROR: Unknown modifier '{}'. Help at `? rlink`.".format(modif)


    def set_active_section(self, section='next'):
        """
        Set specified section active:

        :type section: str
        :param section: Optional parameter. 
                        Accepted values: 
                              'next' (default)  - set next section active
                              'prev' - set previous sectiona active
        
        :rtype: (int, str)
        :return: Returns a tuple rpresenting a return code (int) and a detailed message.
                Return codes are:
                       -1 = no sections in the song
                       -2 = value for 'section' parameter not valid
                       -3 = last section active and cannot activate next
                       -4 = first section active and cannot activare previous
                        0 = OK
        """
        ret_code = 0
        message = ""        
        active_index = self.get_active_section_index()
        if active_index == -1:
            ret_code = -1
            message = "ERROR: You need to add a section before running that command."
        else:
            if section is 'next':
                incr = 1
            elif section is 'prev':
                incr = -1
            else:
                ret_code = -2
                message = "ERROR: Unsupported section value '{}' for song.set_active_section".format(section)
                logger.debug(message)
            # get index for new active section
            new_active_index = active_index + incr
            if new_active_index >= len(self.sections):
                ret_code = -3
                message = "You are at the end of the song."
            elif new_active_index < 0:
                ret_code = -4
                message = "You are at the beginning of the song."
            else:
                # set previous active section inactive and activate the new one
                self.sections[active_index].unset_active()
                self.sections[new_active_index].set_active()
        return (ret_code, message)

    def replace(self, from_text, to_text, global_replace=0):
        """
        Replace a text with another in current section or hole song.
        
        :type from_text: str
        :param from_text: text to replace
        
        :type to_text: str
        :param to_text: text to replace with
        
        :type global_replace: int
        :param global_replace: global modifier. Can only take values 0 or 1:
                                    1 - replace globally
                                    0 - replace only in the current section
        """
        if global_replace == 0:
            active_section = self.get_active_section()
            message = active_section.replace(from_text, to_text)
            if not message:
                message = "Replaced '{}' with '{}' locally (current section)".format(from_text, 
                                                                                  to_text)
        elif global_replace == 1:
            for section in self.sections:
                message = section.replace(from_text, to_text)
                if message: return message
            message = "Replaced '{}' with '{}' globally (entire song)".format(from_text, 
                                                                               to_text)
        else:
            logger.debug('''Unknown value '{}' for global_replace. 
While replacing "{}" with "{}"'''.format(global_replace, from_text, to_text))
            message = '''Error while replacing '{}' with '{}'.
See log for details...'''.format(from_text, to_text)
        return message

    def wordlist(self, path):
        """
        Set custom randomize files path
        """
        if not os.path.isdir(path):
            return "ERROR: Directory '{}' not found.".format(path)
        Globals.RA_PATH = path
        stats_mess = ''
        file_list = os.listdir(path)
        logger.debug("files: {}".format(file_list))
        warning = False
        stats_mess = 'Stats:'
        for index in range(1, Globals.MAX_SYLLABLES+1):
            pattern = "{}syllable*".format(index)
            try:
                file_name = fnmatch.filter(file_list, pattern)[0]
                file_path = os.path.join(path, file_name)
                stats_mess +=" {}: {} lyrics".format(index, 
                                                     count_lines(file_path))
            except Exception as e:
                #IOError or IndexError
                warning = True
                stats_mess += " {}:MISSING".format(index)
                logger.debug("Pattern: {}\nException: {}".format(pattern, e))
        if warning:
            ret_mess = "WARNING: Wordlist '{}' NOT loaded successfully.".format(path)
            end_mess = "WARNING"
        else:
            ret_mess = "SUCCESS: Wordlist '{}' loaded successfully.".format(path)
            end_mess = "OK"
        Globals.WL_STATS = "{} {}. {}".format(ret_mess, stats_mess, end_mess)
        get_ra_wbs()
        return Globals.WL_STATS
            

    def save(self, file_name, save_as_text=False):
        """
        Save song to file.
        """
        try:
            if Globals.SAVED and not file_name:
                file_name = Globals.SAVED                
            file_n, file_e = os.path.splitext(file_name)
            if file_e != '.snd':
                file_name = file_name + '.snd'
            with open(file_name, 'w') as outfile:
                if save_as_text:
                    outfile.write("\t\t{}\n\n".format(self.name.upper()))
                    for section in self.sections:
                        for line in section.lines:
                            outfile.write("\t{}\n".format(line.get_content()))
                        outfile.write("\n")
                    message = "Song saved to plain text file: \n'{}'.".format(file_name)
                else:    
                    pickle.dump(self, outfile)     
                    message = "Song saved to file: \n'{}'.".format(file_name)
                    Globals.SAVED = file_name
        except TypeError as e:
            message = "ERROR: You must specifiy a file to save to."
            log_exception(e)
        except IOError as e:
            message = str(e)
            if 'No such file or directory' in message:
                message = "ERROR: Invalid save path specified."
            elif 'Is a directory' in message:
                message = "ERROR: Please specify a file to save to, not a directory."
            log_exception(e)
        return message

    def load(self, file_name):
        """
        Load song from file 

        file_name - must be a file resultin from a save operation.
        """
        file_n, file_e = os.path.splitext(file_name)
        if file_e != '.snd':
            return '''Invalid extension '{}'. 
   Extension 'snd' expected...'''.format(file_e, file_name)
        try:
            with open(file_name, 'r') as infile:
                Globals.SONG = pickle.load(infile)     
            message = "Song loaded from file '{}'.".format(file_name)
        except TypeError as e:
            message = "ERROR: You must specifiy a file to load from."
            log_exception(e, message)
        except IOError as e:
            message = "ERROR: Specified load file not accessible or does not exitst."
            log_exception(e, message)
        except pickle.UnpicklingError as e:
            message = "ERROR: Specified file is invalid and cannot be loaded."
        return message

    def show(self, modif):
        if modif in ['r', 'rlinks']:
            self.show_rlinks = True
            return "Show rlinks enabled."
        elif modif in ['p', 'prog']:
            locked_sylls = 0
            unlocked_sylls = 0
            for section in self.sections:
                locked, unlocked = section.get_progress()
                locked_sylls += locked
                unlocked_sylls += unlocked
            completed = 100/(locked_sylls + unlocked_sylls) * locked_sylls
            seconds = time() - Globals.START_TIME
            m, s = divmod(seconds, 60)
            h, m = divmod(m, 60)
            return "Song progress: {}% complete ({} mins)".format(completed, int(m))
        elif modif in ['wl', 'wordlist']:
            return Globals.WL_STATS
        elif modif in ['rm', 'rmode']:
            return "Randomize mode is: {}".format(Globals.RMODE)
        else:
            return "ERROR: show - unknown modifier '{}'.".format(modif)

    def hide(self, modif):
        if modif in ['r', 'rlinks']:
            self.show_rlinks = False
            return "Show rlinks disabled."
        else:
            return "ERROR: hide - unknown modifier '{}'.".format(modif)        


