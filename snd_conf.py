import logging
import sys
import os


#put globals into a class in a SEPARATE module to avoid circular imports
class Globals():

    # logger
    LOG_FILE = sys.argv[0].strip("./").split(".")[0] + ".log"
    LOG_FILE_PATH = os.getcwd()
    PRE_LOG = [] # append in case we need to write to log before log init

    # welcome
    INPUT_X = 5
    INPUT_Y = 5
    ERROR_Y = 3
    MAX_LINES = 14
    MAX_SYLLABLES = 20
    MAX_STRUCTURE_CHARS = 14
    MAX_TITLE_CHARS = 35
    MAX_LINE_CHARS = 90
    MAX_CLI_INPUT_CHARS = 50

    # lib
    MIN_TTY_LINES = 45
    MIN_TTY_COLS = MAX_LINE_CHARS + 30
    VALID_SECTIONS = ("verse", "bridge", "chorus", "middle8")
    VALID_SECTION_INITIALS = "VBCM"
    DEFAULTS = { 'name': "UNTITLED SONG",
                 'structure': ("VVCVCMCC", "VCVCVC", "VVBCVBCMCC"),
                 'verse_lines': '4',
                 'verse_syllables': '10',
                 'bridge_lines': '4',
                 'bridge_syllables':'10',
                 'chorus_lines': '4',
                 'chorus_syllables': '7',
                 'middle8_lines': '4',
                 'middle8_syllables': '10'
                 }

    # CLI
    # main window
    STDSCR = None
    # position of title
    TITLE_Y = 3
    TITLE_X = 10
    # position of song structure
    STRUCTURE_X = 60
    # position of section display header
    HEAD_Y = TITLE_Y + 2
    HEAD_X = 7
    # section title x
    SECTION_NAME_X = HEAD_X + 20
    # position of line content display
    LINE_X = HEAD_X + 14
    FIRST_LINE_Y = HEAD_Y + 3
    LINE_DISTANCE = 2
    # position of console prompt
    PROMPT_X = 3

    # do/undo
    SHELF_KEY = 0
    DOUNDO_KEY = -2
    SHELF = 'snd_song_shelf.db'
    SONG_BEFORE_PREV_CMD = None
    PREV_CMD = None
    # commands not to be put in CMD_HISORY even if they change the song
    SKIP_CMD_HISTORY = ('n', 'KEY_RIGHT', 'b', 'KEY_LEFT', 'KEY_UP', 'KEY_DOWN')
    # list containing all entered commands that changed the song
    CMD_HISTORY = list() 
    # corresponding list with database indexes of the song states 
    # SONG_HISTORY[n] = state of the song before running command CMD_HISTORY[n]
    SONG_HISTORY = list()
    # history index used for up and down key handling
    HISTORY_INDEX = 0

    # the song
    START_NEW = True
    SHOW_LINE_NUMBERS = False
    SONG = None

    # remember the last saved filename and use it 
    # in case save with no filename is called
    SAVED = None

    # randomize files path
    RA_PATH = os.path.join(os.getcwd(),'ra')
    WL_STATS = "Using default wordlist: {}".format(RA_PATH)
    # randomize commands list
    RA_COMMANDS = ('ra', 'rl', 'jl')
    # randomize mode
    RMODE = 1
    VALID_RMODES = (1, 2, 3)
    LAST_RMODE_USED = 1 # used for rmode=3 roundrobin

    # temporary - used in get_random_line to ensure proper randomization
    # when looking for a line with a certain number of words
    TEMP_TXT = '/tmp/snd_txt.temp'

    # nltk section
    CMUDICT, CMUDICT_WORDS, CMUDICT_WBS = None, None, None
    
    # dictionaries
    TOP5000_DICT, TOP5000_WORDS, TOP5000_WBS = 'dict/top5000.txt', None, None
    
    # edit mode
    # key pressed when exiting edit mode:
    #   - None = not set
    #   - ESC  = exit without save
    #   - UP   = save modifications and edit above line
    #   - DOWN = -----||-------||----------- line below
    EDIT_EXIT_KEY = 'None'

#####################################################
# Logging HOWTO
# 1. Do not use print. Use the logger object instead by
# importing it in your .py file:
# from config import logger
# 1a. Only use print if you want messages to be displayed
# at the console but not in the log file
# 2. To log message to console and log file use
# log.info(message)
# 3. To log message only to log file (debug messages) use
# log.debug(message)
# 4. Warning (log.warning )and error (log.error) messages
# will be logged to both console and log file.
logger = logging.getLogger('sndLogger')
logger.setLevel(logging.DEBUG)
# log to console
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch_format = logging.Formatter('%(levelname)s: %(message)s')
ch.setFormatter(ch_format)
logger.addHandler(ch)
# log to file
fh = logging.FileHandler(Globals.LOG_FILE)
fh.setLevel(logging.DEBUG)
fh_format = logging.Formatter(
'%(asctime)s %(levelname)s {%(funcName)s->%(filename)s:%(lineno)d}: %(message)s')
fh.setFormatter(fh_format)
logger.addHandler(fh)



